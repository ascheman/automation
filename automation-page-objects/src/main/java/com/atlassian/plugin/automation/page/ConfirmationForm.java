package com.atlassian.plugin.automation.page;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

public class ConfirmationForm
{
    @Inject
    private PageBinder pageBinder;

    @ElementBy(id = "save-rule-form")
    private PageElement saveButton;

    @WaitUntil
    public void isReady()
    {
        waitUntilTrue(saveButton.timed().isPresent());
    }

    public AdminPage save()
    {
        saveButton.click();
        return pageBinder.bind(AdminPage.class);
    }
}

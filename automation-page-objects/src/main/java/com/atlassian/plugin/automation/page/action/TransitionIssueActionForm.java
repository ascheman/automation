package com.atlassian.plugin.automation.page.action;

import com.atlassian.jira.pageobjects.components.fields.SingleSelect;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.plugin.automation.page.ModuleKey;
import org.openqa.selenium.By;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

@ModuleKey ("com.atlassian.plugin.automation.jira-automation-plugin:transition-issue-action")
public class TransitionIssueActionForm extends ActionForm
{

    private SingleSelect transitionSelect;

    public TransitionIssueActionForm(final PageElement container, final String moduleKey)
    {
        super(container, moduleKey);
    }

    @WaitUntil
    public void isInitialised()
    {
        waitUntilTrue(container.find(By.id("jiraActionId-single-select")).timed().isPresent());
    }

    @Init
    public void init()
    {
        final PageElement ssParent = container.find(By.className("action-container"));
        transitionSelect = pageBinder.bind(SingleSelect.class, ssParent);
    }

    public TransitionIssueActionForm selectTransition(final String transition)
    {
        transitionSelect.select(transition);
        return this;
    }

    public TransitionIssueActionForm setTransitionFields(final String fields)
    {
        setActionParam("jiraTransitionFields", fields);
        return this;
    }
}

package com.atlassian.plugin.automation.page;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.plugin.automation.page.util.DialogUtils;
import com.atlassian.webdriver.AtlassianWebDriver;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.openqa.selenium.By;

import java.util.List;
import java.util.Map;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;

public class Rule
{
    private final PageElement row;
    private final PageBinder pageBinder;
    private final PageElementFinder elementFinder;
    private final AtlassianWebDriver driver;

    private final int id;
    private final String name;
    private Trigger trigger;
    private List<Action> actions = Lists.newArrayList();

    public Rule(final PageElement row, final PageBinder pageBinder, final PageElementFinder elementFinder, final AtlassianWebDriver driver)
    {
        this.row = row;
        this.pageBinder = pageBinder;
        this.elementFinder = elementFinder;
        this.driver = driver;

        this.id = Integer.parseInt(row.getAttribute("data-id"));
        final PageElement details = loadRuleDetails();
        this.name = details.find(By.id("automation-name-display")).getText();

        this.trigger = new Trigger(details.find(By.id("trigger-type-display")).getText(), getParams(details, "trigger"));
        extractActions(details);
    }

    public int getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public Status getStatus()
    {
        if (row.find(By.className("aui-icon-error")).isPresent())
        {
            return Status.FAILED;
        }
        if (row.find(By.className("aui-icon-success")).isPresent())
        {
            return Status.OK;
        }
        return Status.UNKNOWN;
    }

    public void delete()
    {
        final PageElement details = loadRuleDetails();
        details.find(By.id("automation-delete")).click();
        DialogUtils.acceptDialog(driver);
    }

    public ConfigRuleForm edit()
    {
        final PageElement details = loadRuleDetails();
        details.find(By.id("automation-edit")).click();
        return pageBinder.bind(ConfigRuleForm.class);
    }

    public ConfigRuleForm copy()
    {
        final PageElement details = loadRuleDetails();
        details.find(By.className("copy-lnk")).click();
        return pageBinder.bind(ConfigRuleForm.class);
    }

    public void enable()
    {
        final PageElement details = loadRuleDetails();
        details.find(By.id("automation-enable")).click();
        DialogUtils.acceptDialog(driver);
        waitUntilFalse(details.find(By.className("throbber")).timed().hasClass("loading"));
    }

    public void disable()
    {
        final PageElement details = loadRuleDetails();
        details.find(By.id("automation-disable")).click();
        DialogUtils.acceptDialog(driver);
        waitUntilFalse(details.find(By.className("throbber")).timed().hasClass("loading"));
    }

    public boolean isEnabled()
    {
        return !row.hasClass("rule-disabled");
    }

    public Trigger getTrigger()
    {
        return trigger;
    }

    public List<Action> getActions()
    {
        return this.actions;
    }

    private PageElement loadRuleDetails()
    {
        row.click();
        return elementFinder.find(By.className("automation-details"));
    }

    private Map<String, String> getParams(final PageElement details, String type)
    {
        final List<PageElement> triggerValues = details.findAll(By.className(type + "-value"));
        final Map<String, String> triggerParams = Maps.newLinkedHashMap();
        for (PageElement triggerValue : triggerValues)
        {
            triggerParams.put(triggerValue.getAttribute("data-key"), triggerValue.getText());
        }
        return triggerParams;
    }

    private void extractActions(final PageElement details)
    {
        final List<PageElement> actionElems = details.findAll(By.className("action-details"));
        for (PageElement actionElem : actionElems)
        {
            final Map<String, String> actionParams = getParams(actionElem, "action");
            this.actions.add(new Action(actionElem.find(By.tagName("h5")).getText(), actionParams));
        }
    }

    public static class Trigger
    {
        private final String type;
        private final Map<String, String> params = Maps.newLinkedHashMap();

        public Trigger(final String type, final Map<String, String> params)
        {
            this.type = type;
            this.params.putAll(params);
        }

        public String getType()
        {
            return type;
        }

        public Map<String, String> getParams()
        {
            return params;
        }
    }

    public static class Action
    {
        private final String type;
        private final Map<String, String> params = Maps.newLinkedHashMap();

        public Action(final String type, final Map<String, String> params)
        {
            this.type = type;
            this.params.putAll(params);
        }

        public String getType()
        {
            return type;
        }

        public Map<String, String> getParams()
        {
            return params;
        }
    }

    public static enum Status
    {
        OK,
        FAILED,
        UNKNOWN;
    }
}

package com.atlassian.plugin.automation.page.action;

import com.atlassian.pageobjects.elements.CheckboxElement;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.plugin.automation.page.ModuleKey;
import org.openqa.selenium.By;

@ModuleKey ("com.atlassian.plugin.automation.jira-automation-plugin:comment-issue-action")
public class CommentIssueActionForm extends ActionForm
{
    public CommentIssueActionForm(final PageElement container, final String moduleKey)
    {
        super(container, moduleKey);
    }

    public CommentIssueActionForm comment(final String comment)
    {
        setActionParam("jiraComment", comment);
        return this;
    }

    public CommentIssueActionForm dispatchEvent(final boolean shouldDispatch)
    {

        final CheckboxElement checkbox = container.find(By.name("jiraCommentNotification"), CheckboxElement.class);
        if (shouldDispatch)
        {
            checkbox.check();
        }
        else
        {
            checkbox.uncheck();
        }
        return this;
    }
}

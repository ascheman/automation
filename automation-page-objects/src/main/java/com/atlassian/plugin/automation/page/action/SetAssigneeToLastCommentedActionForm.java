package com.atlassian.plugin.automation.page.action;

import com.atlassian.jira.pageobjects.components.fields.MultiSelect;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.SelectElement;
import com.atlassian.plugin.automation.page.ModuleKey;
import org.openqa.selenium.By;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

@ModuleKey("com.atlassian.plugin.automation.jira-automation-plugin:set-last-commented-issue-action")
public class SetAssigneeToLastCommentedActionForm extends ActionForm
{
    public SetAssigneeToLastCommentedActionForm(final PageElement container, final String moduleKey)
    {
        super(container, moduleKey);
    }

    public SetAssigneeToLastCommentedActionForm selectGroupRestriction(String group)
    {
        selectValue("groupMembership", group);
        return this;
    }

    public SetAssigneeToLastCommentedActionForm setRestrictedUsers(String... users)
    {
        final PageElement element = container.find(By.name("excludedUsers"));
        waitUntilTrue(element.timed().isPresent());

        final MultiSelect usersMultiSelect = pageBinder.bind(MultiSelect.class, "excludedUsers");
        usersMultiSelect.clearAllItems();
        for (String eventName : users)
        {
            usersMultiSelect.add(eventName);
        }
        return this;
    }

    private void selectValue(final String field, final String value)
    {
        final SelectElement select = container.find(By.name(field), SelectElement.class);
        select.select(Options.value(value));
    }
}

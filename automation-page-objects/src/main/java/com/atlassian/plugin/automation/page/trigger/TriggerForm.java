package com.atlassian.plugin.automation.page.trigger;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.SelectElement;
import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static com.atlassian.jira.pageobjects.form.FormUtils.setElement;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;

/**
 * Represents a trigger form page object.  I know @Matej will want me to combine this with the ActionForm when he looks
 * at this in the pull request review but I've refactored a lot of code in the last 2 days and I'm lazy now so "Be the change you seek!".
 * It's not a huge amount of code duplication anyways :)...
 */
public abstract class TriggerForm
{
    protected final PageElement container;

    @Inject
    protected PageBinder pageBinder;

    /**
     * Module key might be null if no trigger should be set
     */
    public TriggerForm(final PageElement container, final String moduleKey)
    {
        this.container = container;

        // First, wait until normal form is loaded
        waitUntilIsLoaded();

        if (!StringUtils.isBlank(moduleKey))
        {
            // Change selection
            this.container.find(By.id("rule-trigger-module-key"), SelectElement.class).select(Options.value(moduleKey));

            // Wait until after change, the form is loaded
            waitUntilIsLoaded();
        }
    }

    public void waitUntilIsLoaded()
    {
        final PageElement remoteParams = container.find(By.className("remote-params"));
        waitUntilFalse(remoteParams.timed().hasClass("loading-trigger"));
    }

    protected TriggerForm setTriggerParam(final String name, final String value)
    {
        final PageElement element = container.find(By.name(name));
        if (element.getTagName().equals("select"))
        {
            final SelectElement select = container.find(By.name(name), SelectElement.class);
            select.select(Options.value(value));
        }
        else
        {
            setElement(element, value);
        }

        return this;
    }
}

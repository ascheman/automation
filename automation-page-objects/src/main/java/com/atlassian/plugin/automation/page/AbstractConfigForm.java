package com.atlassian.plugin.automation.page;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.AbstractTimedCondition;
import com.atlassian.plugin.automation.page.util.ValidationUtils;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

public abstract class AbstractConfigForm
{
    @ElementBy(id = "progress-rule-form")
    protected PageElement nextButton;

    @ElementBy(id = "add-rule-cancel")
    protected PageElement cancelButton;

    @Inject
    protected PageBinder pageBinder;

    @Inject
    protected PageElementFinder elementFinder;

    public AdminPage cancel()
    {
        cancelButton.click();
        return pageBinder.bind(AdminPage.class);
    }

    public Iterable<String> nextExpectError()
    {
        nextButton.click();
        waitUntilTrue(new ErrorsShownCondition(elementFinder));
        return ValidationUtils.getAllErrors(elementFinder);
    }

    public static class ErrorsShownCondition extends AbstractTimedCondition
    {
        private static final int MAX_TIMEOUT = 15000;
        private static final int POLLING_INTERVAL = 500;
        private final PageElementFinder elementFinder;

        public ErrorsShownCondition(final PageElementFinder elementFinder)
        {
            super(MAX_TIMEOUT, POLLING_INTERVAL);
            this.elementFinder = elementFinder;
        }

        @Override
        protected Boolean currentValue()
        {
            final int errors = elementFinder.findAll(By.className("error")).size();
            return errors > 0;
        }
    }
}

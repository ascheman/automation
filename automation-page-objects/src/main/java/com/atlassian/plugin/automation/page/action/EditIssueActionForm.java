package com.atlassian.plugin.automation.page.action;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.plugin.automation.page.ModuleKey;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

@ModuleKey ("com.atlassian.plugin.automation.jira-automation-plugin:edit-issue-action")
public class EditIssueActionForm extends ActionForm
{
    public EditIssueActionForm(final PageElement container, final String moduleKey)
    {
        super(container, moduleKey);
    }

    public EditIssueActionForm setTransitionFields(final String fields)
    {
        setActionParam("jiraEditFields", fields);
        return this;
    }
}

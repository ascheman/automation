package com.atlassian.plugin.automation.config;

/**
 * This class contains setting keys used to access via PluginSettings
 */
public final class Settings
{
    /**
     * Enables/disables rules rate limiter
     */
    public static final String LIMITER_ENABLED = "automation.limiter.enabled";

    private Settings()
    {
    }
}

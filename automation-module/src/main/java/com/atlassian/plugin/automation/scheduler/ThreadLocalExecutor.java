package com.atlassian.plugin.automation.scheduler;

import com.atlassian.plugin.automation.spi.ThreadLocalContextProvider;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.log4j.Logger;

import java.util.concurrent.Callable;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Runs a callable as a local task, using given actor
 */
@Named
public class ThreadLocalExecutor
{
    private static final Logger log = Logger.getLogger(ThreadLocalExecutor.class);
    private final ThreadLocalContextProvider threadLocalContextProvider;

    @Inject
    public ThreadLocalExecutor(@ComponentImport ThreadLocalContextProvider threadLocalContextProvider)
    {
        this.threadLocalContextProvider = threadLocalContextProvider;
    }

    public void executeAs(String actor, Callable<ErrorCollection> callable)
    {
        threadLocalContextProvider.preCall(actor);
        try
        {
            callable.call();
        }
        catch (Exception e)
        {
            log.error("Exception while running callable", e);
        }
        finally
        {
            threadLocalContextProvider.postCall(actor);
        }
    }
}

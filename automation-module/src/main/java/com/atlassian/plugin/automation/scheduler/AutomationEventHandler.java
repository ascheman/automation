package com.atlassian.plugin.automation.scheduler;

public interface AutomationEventHandler
{
    public void handleEvent(final Object event);
}

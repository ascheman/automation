package com.atlassian.plugin.automation.module;

import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.osgi.external.SingleModuleDescriptorFactory;

public class AutomationActionModuleDescriptorFactory
        extends SingleModuleDescriptorFactory<AutomationActionModuleDescriptor>
{
    public AutomationActionModuleDescriptorFactory(final HostContainer hostContainer)
    {
        super(hostContainer, "automation-action", AutomationActionModuleDescriptor.class);
    }
}

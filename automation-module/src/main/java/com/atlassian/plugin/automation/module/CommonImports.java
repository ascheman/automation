package com.atlassian.plugin.automation.module;

import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Transitive imports required by this module.
 */
@Named
public class CommonImports
{
    private final ModuleFactory moduleFactory;

    @Inject
    public CommonImports(@ComponentImport ModuleFactory moduleFactory)
    {
        this.moduleFactory = moduleFactory;
    }
}

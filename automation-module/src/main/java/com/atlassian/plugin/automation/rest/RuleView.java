package com.atlassian.plugin.automation.rest;

import com.google.common.collect.Lists;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

/**
 * Represents a renderable JSON Rule.
 */
public class RuleView
{
    @JsonProperty
    private RuleWithStatusWrapper rule;

    @JsonProperty
    private ViewContainer triggerView;

    @JsonProperty
    private List<ViewContainer> actionViews = Lists.newArrayList();

    @JsonCreator
    public RuleView(@JsonProperty ("rule") final RuleWithStatusWrapper rule,
            @JsonProperty ("triggerView") final ViewContainer triggerView,
            @JsonProperty ("actionViews") List<ViewContainer> actionViews)
    {
        this.rule = rule;
        this.triggerView = triggerView;
        this.actionViews.addAll(actionViews);
    }

    public RuleWithStatusWrapper getRule()
    {
        return rule;
    }

    public ViewContainer getTriggerView()
    {
        return triggerView;
    }

    public List<ViewContainer> getActionViews()
    {
        return actionViews;
    }
}

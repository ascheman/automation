package com.atlassian.plugin.automation.status;

public interface RuleStatusService
{
    /**
     * Sets the status of given rule
     * @param ruleId
     * @param status
     */
    void setRuleStatus(final int ruleId, final RuleStatus status);

    /**
     * Gets the last knwon status of the rule or "UNKNOWN" if the rule with given ID never executed
     * @param ruleId
     * @return
     */
    RuleStatus getRuleStatus(final int ruleId);
}

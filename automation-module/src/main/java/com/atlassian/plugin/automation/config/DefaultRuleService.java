package com.atlassian.plugin.automation.config;

import com.atlassian.fugue.Either;
import com.atlassian.plugin.automation.core.Action;
import com.atlassian.plugin.automation.core.CronTrigger;
import com.atlassian.plugin.automation.core.EventTrigger;
import com.atlassian.plugin.automation.core.Rule;
import com.atlassian.plugin.automation.core.Trigger;
import com.atlassian.plugin.automation.core.action.ActionConfiguration;
import com.atlassian.plugin.automation.core.auditlog.AuditMessageBuilder;
import com.atlassian.plugin.automation.core.trigger.TriggerConfiguration;
import com.atlassian.plugin.automation.module.AutomationModuleManager;
import com.atlassian.plugin.automation.scheduler.AutomationScheduler;
import com.atlassian.plugin.automation.service.AuditLogService;
import com.atlassian.plugin.automation.service.RuleErrors;
import com.atlassian.plugin.automation.service.RuleService;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserManager;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;

import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ExportAsService
public class DefaultRuleService implements RuleService
{
    private final I18nResolver i18n;
    private final AuditLogService auditLogService;
    private final UserManager userManager;
    private final AutomationScheduler automationScheduler;
    private final RuleConfigStore store;
    private final AutomationModuleManager moduleManager;

    @Inject
    public DefaultRuleService(
            @ComponentImport final I18nResolver i18n,
            @ComponentImport final UserManager userManager,
            final AuditLogService auditLogService,
            final AutomationScheduler automationScheduler,
            final RuleConfigStore store,
            final AutomationModuleManager moduleManager)
    {
        this.i18n = i18n;
        this.auditLogService = auditLogService;
        this.userManager = userManager;
        this.automationScheduler = automationScheduler;
        this.store = store;
        this.moduleManager = moduleManager;
    }

    @Override
    public Either<ErrorCollection, Iterable<Rule>> getRules(String loggedInUser)
    {
        final ErrorCollection errors = new ErrorCollection();
        checkPermissions(loggedInUser, errors);
        if (errors.hasAnyErrors())
        {
            return Either.left(errors);
        }
        return Either.right(store.getRules());
    }

    @Override
    public Either<ErrorCollection, Rule> getRule(String loggedInUser, int ruleId)
    {
        final Rule rule = store.getRule(ruleId);
        if (rule == null)
        {
            ErrorCollection errorCollection = new ErrorCollection();
            errorCollection.addErrorMessage(i18n.getText("not.found.desc"), ErrorCollection.Reason.NOT_FOUND);
            return Either.left(errorCollection);
        }
        return Either.right(rule);
    }

    @Override
    public Either<RuleErrors, RuleValidationResult> validateAddRule(String loggedInUser, Rule rule)
    {
        final RuleErrors errors = validateCompleteRule(loggedInUser, rule);

        if (errors.hasAnyErrors())
        {
            return Either.left(errors);
        }

        return Either.right(new RuleValidationResult(rule));
    }

    @Override
    public Rule addRule(RuleValidationResult ruleValidationResult)
    {
        final Rule rule = store.addRule(ruleValidationResult.getRule());
        automationScheduler.scheduleRule(rule, this);

        auditLogService.addEntry(new AuditMessageBuilder().
                setActor(userManager.getRemoteUsername()).
                setRuleId(rule.getId()).
                setMessage("Added new rule with id " + rule.getId()).
                build());
        return rule;
    }

    @Override
    public Either<ErrorCollection, DeleteRuleValidationResult> validateDeleteRule(String loggedInUser, int ruleId)
    {
        final ErrorCollection errors = new ErrorCollection();
        checkPermissions(loggedInUser, errors);
        final Rule existingRule = store.getRule(ruleId);

        if (existingRule == null)
        {
            errors.addErrorMessage(i18n.getText("not.found.desc"), ErrorCollection.Reason.NOT_FOUND);
        }
        if (errors.hasAnyErrors())
        {
            return Either.left(errors);
        }
        return Either.right(new DeleteRuleValidationResult(ruleId));
    }

    @Override
    public void deleteRule(DeleteRuleValidationResult deleteRuleValidationResult)
    {
        final Rule deletedRule = store.delete(deleteRuleValidationResult.getRuleId());

        // Don't forget to unschedule the rule
        automationScheduler.unscheduleRule(deletedRule);
        auditLogService.addEntry(new AuditMessageBuilder().
                setActor(userManager.getRemoteUsername()).
                setRuleId(deleteRuleValidationResult.getRuleId()).
                setMessage("Deleted rule with id " + deleteRuleValidationResult.getRuleId()).
                build());
    }

    private void checkPermissions(String loggedInUser, ErrorCollection errors)
    {
        if (StringUtils.isBlank(loggedInUser))
        {
            errors.addErrorMessage(i18n.getText("not.logged.in.desc"), ErrorCollection.Reason.NOT_LOGGED_IN);
        }

        if (!userManager.isSystemAdmin(loggedInUser))
        {
            errors.addErrorMessage(i18n.getText("perm.denied.desc"), ErrorCollection.Reason.FORBIDDEN);
        }
    }

    @Override
    public Either<ErrorCollection, UpdateRuleStatusValidationResult> validateUpdateRuleStatus(String loggedInUser, int ruleId)
    {
        final ErrorCollection errors = new ErrorCollection();
        checkPermissions(loggedInUser, errors);
        final Rule existingRule = store.getRule(ruleId);

        if (existingRule == null)
        {
            errors.addErrorMessage(i18n.getText("not.found.desc"), ErrorCollection.Reason.NOT_FOUND);
        }
        if (errors.hasAnyErrors())
        {
            return Either.left(errors);
        }
        return Either.right(new UpdateRuleStatusValidationResult(ruleId));
    }

    @Override
    public Rule updateRuleStatus(UpdateRuleStatusValidationResult updateStatusValidationResult, boolean status)
    {
        final Rule updatedRule = store.updateRuleStatus(updateStatusValidationResult.getRuleId(), status);
        final String action = updatedRule.isEnabled() ? "Enabled" : "Disabled";
        auditLogService.addEntry(new AuditMessageBuilder().
                setActor(userManager.getRemoteUsername()).
                setRuleId(updateStatusValidationResult.getRuleId()).
                setMessage(String.format("%s rule with id %d", action, updateStatusValidationResult.getRuleId())).
                build());

        if (updatedRule.isEnabled())
        {
            automationScheduler.scheduleRule(updatedRule, this);
        }
        else
        {
            automationScheduler.unscheduleRule(updatedRule);
        }

        return updatedRule;
    }

    @Override
    public Either<RuleErrors, RuleValidationResult> validateUpdateRule(String remoteUsername, Rule rule)
    {
        final Either<ErrorCollection, Rule> getRuleResult = getRule(remoteUsername, rule.getId());
        if (getRuleResult.isLeft())
        {
            return Either.left(new RuleErrors(getRuleResult.left().get(), new ErrorCollection(), Lists.<ErrorCollection>newArrayList()));
        }
        final RuleErrors ruleErrors = validateCompleteRule(remoteUsername, rule);
        if (ruleErrors.hasAnyErrors())
        {
            return Either.left(ruleErrors);
        }
        return Either.right(new RuleValidationResult(rule));
    }

    @Override
    public Rule updateRule(RuleValidationResult ruleValidationResult)
    {
        automationScheduler.unscheduleRule(ruleValidationResult.getRule());
        final Rule rule = store.updateRule(ruleValidationResult.getRule());
        automationScheduler.scheduleRule(rule, this);

        auditLogService.addEntry(new AuditMessageBuilder().
                setActor(userManager.getRemoteUsername()).
                setRuleId(rule.getId()).
                setMessage("Updated rule with id " + rule.getId()).
                build());
        return rule;
    }

    @Override
    public RuleErrors validatePartialRule(final String loggedInUser, final Rule rule, int step)
    {
        final ErrorCollection emptyErrors = new ErrorCollection();
        final List<ErrorCollection> emptyActionErrors = Lists.newArrayList();

        switch (step)
        {
            case 0:
                return new RuleErrors(validateRule(rule), emptyErrors, emptyActionErrors);
            case 1:
                return new RuleErrors(emptyErrors, validateTrigger(rule), emptyActionErrors);
            case 2:
                return new RuleErrors(emptyErrors, emptyErrors, validateActions(rule));
            default:
                final ErrorCollection stepErrors = new ErrorCollection();
                stepErrors.addErrorMessage(i18n.getText("rule.validation.step.invalid"), ErrorCollection.Reason.NOT_FOUND);
                return new RuleErrors(stepErrors);
        }
    }

    private RuleErrors validateCompleteRule(String loggedInUser, Rule rule)
    {
        final ErrorCollection permissionErrors = new ErrorCollection();
        checkPermissions(loggedInUser, permissionErrors);
        if (permissionErrors.hasAnyErrors())
        {
            return new RuleErrors(permissionErrors);
        }

        final ErrorCollection ruleErrors = validateRule(rule);
        final ErrorCollection triggerErrors = validateTrigger(rule);
        final List<ErrorCollection> actionErrors = validateActions(rule);
        return new RuleErrors(ruleErrors, triggerErrors, actionErrors);
    }

    private List<ErrorCollection> validateActions(Rule rule)
    {
        final List<ErrorCollection> actionErrors = Lists.newArrayList();
        Iterable<ActionConfiguration> actionsConfiguration = rule.getActionsConfiguration();
        if (Iterables.isEmpty(actionsConfiguration))
        {
            final ErrorCollection firstActionErrors = new ErrorCollection();
            firstActionErrors.addError("moduleKey", i18n.getText("rule.action.missing"));
            actionErrors.add(firstActionErrors);
        }

        for (ActionConfiguration actionConfig : actionsConfiguration)
        {
            final ErrorCollection actionError = new ErrorCollection();
            final String actionModuleKey = actionConfig.getModuleKey();
            final Action action = moduleManager.getAction(actionModuleKey);

            if (action == null)
            {
                actionError.addError("moduleKey", i18n.getText("rule.action.invalid"));
            }
            else
            {
                final Map<String, List<String>> actionParamMap = actionConfig.getParameters();
                actionError.addErrorCollection(action.validateAddConfiguration(i18n, actionParamMap, rule.getActor()));
            }
            actionErrors.add(actionError);
        }
        return actionErrors;
    }

    private ErrorCollection validateTrigger(Rule rule)
    {
        final ErrorCollection triggerErrors = new ErrorCollection();
        final TriggerConfiguration triggerConfig = rule.getTriggerConfiguration();
        final String triggerModuleKey = triggerConfig.getModuleKey();
        if (StringUtils.isNotBlank(triggerModuleKey))
        {
            //trigger module was provided.  Lets validate this trigger!
            final Trigger trigger = moduleManager.getTrigger(triggerModuleKey);
            if (trigger == null)
            {
                triggerErrors.addError("moduleKey", i18n.getText("rule.trigger.invalid"));
            }
            else
            {
                trigger.init(triggerConfig);
                final Map<String, List<String>> triggerParamMap = triggerConfig.getParameters();
                if (!(trigger instanceof EventTrigger) && !(trigger instanceof CronTrigger))
                {
                    //we only support CRON or Event based triggers currently!
                    triggerErrors.addError("moduleKey", i18n.getText("rule.trigger.invalid"));
                }

                triggerErrors.addErrorCollection(trigger.validateAddConfiguration(i18n, triggerParamMap, rule.getActor()));
            }
        }
        else
        {
            //if we're note doing a partial validation then the trigger is mandatory!
            triggerErrors.addError("moduleKey", i18n.getText("rule.trigger.missing"));
        }
        return triggerErrors;
    }

    private ErrorCollection validateRule(Rule rule)
    {
        final ErrorCollection ruleErrors = new ErrorCollection();

        // Validate rule name
        final String ruleName = rule.getName();
        if (StringUtils.isBlank(ruleName))
        {
            ruleErrors.addError("name", i18n.getText("rule.name.invalid"));
        }

        final Rule ruleByName = store.getRule(ruleName);
        //if we're updating and the ids of the rule are the same then ignore the fact that
        //the name already exists (updating but not changing the name)
        if (ruleByName != null && rule.getId() != ruleByName.getId())
        {
            ruleErrors.addError("name", i18n.getText("rule.name.exists"));
        }

        // Validate actor
        final String actor = rule.getActor();
        if (StringUtils.isBlank(actor) ||
                userManager.getUserProfile(actor) == null)
        {
            ruleErrors.addError("actor", i18n.getText("rule.actor.invalid"));
        }
        return ruleErrors;
    }
}

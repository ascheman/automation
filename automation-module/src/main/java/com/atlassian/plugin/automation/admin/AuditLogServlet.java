package com.atlassian.plugin.automation.admin;

import com.atlassian.fugue.Either;
import com.atlassian.plugin.automation.config.RuleConfigStore;
import com.atlassian.plugin.automation.core.Rule;
import com.atlassian.plugin.automation.core.auditlog.AuditMessage;
import com.atlassian.plugin.automation.service.AuditLogService;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.timezone.TimeZoneManager;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.google.common.collect.Maps.newHashMap;

public class AuditLogServlet extends AbstractAdminServlet
{
    private static final int PAGE_SIZE = 100;
    private static final int MAX_NUM_PAGES = 10;

    private final AuditLogService auditLogService;
    private final RuleConfigStore ruleStore;
    private final TimeZoneManager timeZoneManager;

    @Inject
    public AuditLogServlet(
            @ComponentImport final WebSudoManager webSudoManager,
            @ComponentImport final SoyTemplateRenderer renderer,
            @ComponentImport final UserManager userManager,
            @ComponentImport final LoginUriProvider loginUriProvider,
            @ComponentImport final WebResourceManager webResourceManager,
            @ComponentImport final TimeZoneManager timeZoneManager,
            final AuditLogService auditLogService,
            final RuleConfigStore ruleStore)
    {
        super(webSudoManager, renderer, userManager, loginUriProvider, webResourceManager);
        this.auditLogService = auditLogService;
        this.ruleStore = ruleStore;
        this.timeZoneManager = timeZoneManager;
    }

    @Override
    protected void requireResource(WebResourceManager webResourceManager)
    {
        webResourceManager.requireResource(RuleConfigServlet.ADMIN_RESOURCES);
    }

    @Override
    protected void renderResponse(SoyTemplateRenderer renderer, HttpServletRequest request, HttpServletResponse response)
            throws IOException, SoyException
    {
        final Map<String, Object> context = newHashMap();

        int startAt = getNumberParam(request, "startAt", 0);
        int pageSize = Math.min(999, getNumberParam(request, "pageSize", PAGE_SIZE));

        int total = auditLogService.getEntriesCount(request.getRemoteUser());

        final Either<ErrorCollection, Iterable<AuditMessage>> auditResult = auditLogService.getAllEntries(request.getRemoteUser(), startAt, pageSize);
        if (auditResult.isLeft())
        {
            context.put("errors", auditResult.left().get());
            renderer.render(response.getWriter(), CONFIG_RESOURCE_KEY, "Atlassian.Templates.Automation.configError", context);
            return;
        }

        final Map<Integer, String> ruleNames = Maps.newHashMap();
        for (Rule rule : ruleStore.getRules())
        {
            ruleNames.put(rule.getId(), rule.getName());
        }

        final Iterable<AuditMessage> auditMessages = auditResult.right().get();
        final List<String> ruleNamesList = Lists.newArrayList();
        for (AuditMessage auditMessage : auditMessages)
        {
            String ruleName = ruleNames.get(auditMessage.getRuleId());
            if (StringUtils.isBlank(ruleName))
            {
                ruleName = String.format("(deleted:%d)", auditMessage.getRuleId());
            }
            ruleNamesList.add(ruleName);
        }

        final List<Page> pages = getPages(startAt, total, pageSize);
        Page currentPage = null;
        if (total > 0)
        {
            currentPage = Iterables.find(pages, new Predicate<Page>()
            {

                @Override
                public boolean apply(@Nullable final Page input)
                {
                    return input != null && input.isCurrent();
                }
            }, pages.get(0));
        }


        context.put("total", total);
        context.put("pageSize", pageSize);
        context.put("currentPage", currentPage);
        context.put("pages", pages);
        context.put("ruleNames", ruleNamesList);
        context.put("auditMessages", auditMessages);
        context.put("timezone", timeZoneManager.getUserTimeZone());
        renderer.render(response.getWriter(), CONFIG_RESOURCE_KEY, "Atlassian.Templates.Automation.showAuditLog", context);
    }

    private int getNumberParam(final HttpServletRequest request, String paramKey, int defaultValue)
    {
        final String startAtString = request.getParameter(paramKey);
        int num = defaultValue;
        if (StringUtils.isNotBlank(startAtString) && StringUtils.isNumeric(startAtString))
        {
            num = Integer.parseInt(startAtString);
        }
        return num;
    }

    private List<Page> getPages(final int startAt, final int total, final int pageSize)
    {
        final List<Page> pages = Lists.newArrayList();
        int i = 0;
        int currentIndex = 0;
        int current = 0;

        pages.add(new Page(++i, current, Math.min(current + pageSize, total), startAt == 0));

        while (current < (total - pageSize))
        {
            current = i * pageSize;
            boolean isCurrent = startAt == current;
            if (isCurrent)
            {
                currentIndex = i;
            }
            pages.add(new Page(++i, current, Math.min(current + pageSize, total), isCurrent));
        }

        if (pages.size() > MAX_NUM_PAGES)
        {
            final int start = Math.max(0, currentIndex - MAX_NUM_PAGES / 2);
            return pages.subList(start, Math.min(pages.size(), start + MAX_NUM_PAGES));
        }

        return pages;
    }

    public static class Page
    {
        private final int pageIndex;
        private final int start;
        private final int end;
        private final boolean current;

        public Page(final int pageIndex, final int start, final int end, final boolean current)
        {
            this.pageIndex = pageIndex;
            this.start = start;
            this.current = current;
            this.end = end;
        }

        public int getPageIndex()
        {
            return pageIndex;
        }

        public int getStart()
        {
            return start;
        }

        public boolean isCurrent()
        {
            return current;
        }

        public int getEnd()
        {
            return end;
        }
    }
}
package com.atlassian.plugin.automation.upgrade;

import com.atlassian.plugin.automation.config.Settings;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.Collections;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * We added settings, so this task just sets the defaults
 *
 * @since v5.0
 */
@Named
@ExportAsService
public class UpgradeSettings implements PluginUpgradeTask
{
    private static final Logger log = Logger.getLogger(UpgradeSettings.class);
    private final PluginSettings pluginSettings;

    @Inject
    public UpgradeSettings(@ComponentImport final PluginSettingsFactory pluginSettingsFactory)
    {
        pluginSettings = pluginSettingsFactory.createGlobalSettings();
    }

    @Override
    public int getBuildNumber()
    {
        return 2;
    }

    @Override
    public String getShortDescription()
    {
        return "Upgrades settings with default values.";
    }

    @Override
    public Collection<Message> doUpgrade() throws Exception
    {
        pluginSettings.put(Settings.LIMITER_ENABLED, String.valueOf(true));
        return Collections.emptyList();
    }

    @Override
    public String getPluginKey()
    {
        return "com.atlassian.plugin.automation.automation-module";
    }
}
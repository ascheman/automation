package com.atlassian.plugin.automation.scheduler;

import com.atlassian.event.api.EventListener;
import com.atlassian.plugin.automation.core.Rule;
import com.atlassian.plugin.automation.core.TriggerContext;
import com.atlassian.sal.api.transaction.TransactionCallback;
import org.apache.log4j.Logger;

import java.util.concurrent.ExecutorService;

public class DefaultAutomationEventHandler implements AutomationEventHandler
{
    private static final Logger log = Logger.getLogger(DefaultAutomationEventHandler.class);

    private final ExecutorService eventExecutor;
    private final String eventClassName;
    private final RuleCallableContext ruleContext;

    public DefaultAutomationEventHandler(final RuleCallableContext ruleContext,
                                         final ExecutorService eventExecutor,
                                         final String eventClassName)
    {
        this.ruleContext = ruleContext;
        this.eventExecutor = eventExecutor;
        this.eventClassName = eventClassName;
    }

    @EventListener
    @Override
    public void handleEvent(final Object event)
    {
        final Rule rule = ruleContext.getRule();
        if (event.getClass().getCanonicalName().equals(eventClassName))
        {
            // Execute the rule asynchronously due to possible performance issues
            eventExecutor.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    ruleContext.getTransactionTemplate().execute(new TransactionCallback<Object>()
                    {
                        @Override
                        public Object doInTransaction()
                        {
                            try
                            {
                                ruleContext.getThreadLocalExecutor().executeAs(rule.getActor(), new RuleCallable(ruleContext,
                                        new TriggerContext(rule.getActor(), event)));
                            }

                            catch (RuntimeException e)
                            {
                                log.error("Unexpected error executing automation rule '" + rule.getName() + "'", e);
                            }
                            return null;
                        }
                    });
                }
            });

        }
    }
}

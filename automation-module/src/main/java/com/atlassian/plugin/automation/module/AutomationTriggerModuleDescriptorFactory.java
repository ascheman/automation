package com.atlassian.plugin.automation.module;

import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.osgi.external.SingleModuleDescriptorFactory;

public class AutomationTriggerModuleDescriptorFactory
        extends SingleModuleDescriptorFactory<AutomationTriggerModuleDescriptor>
{
    public AutomationTriggerModuleDescriptorFactory(final HostContainer hostContainer)
    {
        super(hostContainer, "automation-trigger", AutomationTriggerModuleDescriptor.class);
    }
}

package com.atlassian.plugin.automation.scheduler;

import com.atlassian.plugin.automation.core.Rule;
import com.atlassian.plugin.automation.module.AutomationModuleManager;
import com.atlassian.plugin.automation.service.AuditLogService;
import com.atlassian.plugin.automation.service.RuleService;
import com.atlassian.sal.api.transaction.TransactionTemplate;

class RuleCallableContext
{
    private final Rule rule;
    private final ThreadLocalExecutor threadLocalExecutor;
    private final AuditLogService auditLogService;
    private final TransactionTemplate transactionTemplate;
    private final AutomationModuleManager automationModuleManager;
    private final RuleExecutionLimiter ruleExecutionLimiter;
    private final RuleService ruleService;

    RuleCallableContext(final Rule rule, final ThreadLocalExecutor threadLocalExecutor, final AuditLogService auditLogService, final TransactionTemplate transactionTemplate,
                        final AutomationModuleManager automationModuleManager, final RuleExecutionLimiter ruleExecutionLimiter, final RuleService ruleService)
    {
        this.rule = rule;
        this.threadLocalExecutor = threadLocalExecutor;
        this.auditLogService = auditLogService;
        this.transactionTemplate = transactionTemplate;
        this.automationModuleManager = automationModuleManager;
        this.ruleExecutionLimiter = ruleExecutionLimiter;
        this.ruleService = ruleService;
    }

    RuleService getRuleService()
    {
        return ruleService;
    }

    Rule getRule()
    {
        return rule;
    }

    ThreadLocalExecutor getThreadLocalExecutor()
    {
        return threadLocalExecutor;
    }

    AuditLogService getAuditLogService()
    {
        return auditLogService;
    }

    TransactionTemplate getTransactionTemplate()
    {
        return transactionTemplate;
    }

    AutomationModuleManager getAutomationModuleManager()
    {
        return automationModuleManager;
    }

    RuleExecutionLimiter getRuleExecutionLimiter()
    {
        return ruleExecutionLimiter;
    }
}

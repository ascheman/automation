package com.atlassian.plugin.automation.admin;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;

public class RuleConfigServlet extends AbstractAdminServlet
{
    public static final String ADMIN_RESOURCES = COMPLETE_PLUGIN_KEY + ":rule-admin-resources";
    public static final String AUTOMATION_WEB_RESOURCE_CONTEXT = "com.atlassian.automation.configform";

    @Inject
    public RuleConfigServlet(
            @ComponentImport final WebSudoManager webSudoManager,
            @ComponentImport final SoyTemplateRenderer renderer,
            @ComponentImport final UserManager userManager,
            @ComponentImport final LoginUriProvider loginUriProvider,
            @ComponentImport final WebResourceManager webResourceManager)
    {
        super(webSudoManager, renderer, userManager, loginUriProvider, webResourceManager);
    }

    @Override
    protected void requireResource(WebResourceManager webResourceManager)
    {
        webResourceManager.requireResource(ADMIN_RESOURCES);
        webResourceManager.requireResourcesForContext(AUTOMATION_WEB_RESOURCE_CONTEXT);
    }

    @Override
    protected void renderResponse(SoyTemplateRenderer renderer, HttpServletRequest request, HttpServletResponse response) throws IOException, SoyException
    {
        final Map<String, Object> context = newHashMap();
        renderer.render(response.getWriter(), CONFIG_RESOURCE_KEY, "Atlassian.Templates.Automation.ruleConfig", context);
    }
}
package com.atlassian.plugin.automation.module;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.automation.core.Action;
import com.atlassian.plugin.automation.core.Trigger;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.tracker.PluginModuleTracker;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Named;

import static com.atlassian.plugin.tracker.DefaultPluginModuleTracker.create;

@Named
public class DefaultAutomationModuleManager implements AutomationModuleManager
{
    private final PluginModuleTracker<Trigger, AutomationTriggerModuleDescriptor> triggerTracker;
    private final PluginModuleTracker<Action, AutomationActionModuleDescriptor> actionTracker;

    @Inject
    public DefaultAutomationModuleManager(
            @ComponentImport final PluginAccessor pluginAccessor,
            @ComponentImport final PluginEventManager pluginEventManager)
    {
        triggerTracker = create(pluginAccessor, pluginEventManager, AutomationTriggerModuleDescriptor.class);
        actionTracker = create(pluginAccessor, pluginEventManager, AutomationActionModuleDescriptor.class);
    }

    @Override
    public Trigger getTrigger(final String key)
    {
        final AutomationTriggerModuleDescriptor triggerModuleDescriptor = Iterables.find(triggerTracker.getModuleDescriptors(),
                new Predicate<AutomationTriggerModuleDescriptor>()
                {
                    @Override
                    public boolean apply(@Nullable AutomationTriggerModuleDescriptor triggerModuleDescriptor)
                    {
                        return triggerModuleDescriptor != null && triggerModuleDescriptor.getCompleteKey().equals(key);
                    }
                }, null);
        if (triggerModuleDescriptor != null)
        {
            return triggerModuleDescriptor.getModule();
        }
        return null;
    }

    @Override
    public Action getAction(final String key)
    {
        final AutomationActionModuleDescriptor triggerModuleDescriptor = Iterables.find(actionTracker.getModuleDescriptors(),
                new Predicate<AutomationActionModuleDescriptor>()
                {
                    @Override
                    public boolean apply(@Nullable AutomationActionModuleDescriptor actionModuleDescriptor)
                    {
                        return actionModuleDescriptor != null&& actionModuleDescriptor.getCompleteKey().equals(key);
                    }
                }, null);
        if (triggerModuleDescriptor != null)
        {
            return triggerModuleDescriptor.getModule();
        }
        return null;
    }

    @Override
    public Iterable<AutomationTriggerModuleDescriptor> getTriggerDescriptors()
    {
        return triggerTracker.getModuleDescriptors();
    }

    @Override
    public Iterable<AutomationActionModuleDescriptor> getActionDescriptors()
    {
        return actionTracker.getModuleDescriptors();
    }
}

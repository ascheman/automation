package com.atlassian.plugin.automation.scheduler;

import com.atlassian.fugue.Either;
import com.atlassian.plugin.automation.core.Action;
import com.atlassian.plugin.automation.core.Rule;
import com.atlassian.plugin.automation.core.Trigger;
import com.atlassian.plugin.automation.core.TriggerContext;
import com.atlassian.plugin.automation.core.action.ActionConfiguration;
import com.atlassian.plugin.automation.core.auditlog.AuditMessageBuilder;
import com.atlassian.plugin.automation.service.RuleService;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.concurrent.Callable;

class RuleCallable implements Callable<ErrorCollection>
{
    private static final Logger log = Logger.getLogger(RuleCallable.class);
    private static final int MAX_ERRORS = 3;

    private final RuleCallableContext context;
    private final TriggerContext triggerContext;

    public RuleCallable(final RuleCallableContext context, final TriggerContext triggerContext)
    {
        this.context = context;
        this.triggerContext = triggerContext;
    }

    @Override
    public ErrorCollection call() throws Exception
    {
        final Rule rule = context.getRule();
        log.debug("Executing rule: " + rule.getName());
        final ErrorCollection ruleErrors = new ErrorCollection();

        final Trigger trigger = context.getAutomationModuleManager().getTrigger(rule.getTriggerConfiguration().getModuleKey());
        if (trigger != null)
        {
            try
            {
                trigger.init(rule.getTriggerConfiguration());
                final Iterable<?> items = trigger.getItems(triggerContext, ruleErrors);
                if (!Iterables.isEmpty(items))
                {
                    // We need to check HERE if we can execute the actions - event triggers get triggered on the event class itself,
                    // but that does not necessarily mean they return items too
                    if (!context.getRuleExecutionLimiter().canExecuteActions(rule, trigger.getAuditLog().getString()))
                    {
                        return disableRuleAndReturn(rule);
                    }

                    final List<String> actionLogs = Lists.newArrayList();
                    for (ActionConfiguration actionConfiguration : rule.getActionsConfiguration())
                    {
                        final Action action = context.getAutomationModuleManager().getAction(actionConfiguration.getModuleKey());
                        if (action != null)
                        {
                            action.init(actionConfiguration);
                            action.execute(rule.getActor(), items, ruleErrors);
                            actionLogs.add(action.getAuditLog().getString());
                        }
                        else
                        {

                            log.error("No action with module key '" + actionConfiguration.getModuleKey() +
                                    "' could be loaded for rule '" + rule.getName() + "'. Is the plugin providing this action still installed?");
                            context.getAuditLogService().addEntry(new AuditMessageBuilder().
                                    setActor(rule.getActor()).
                                    setRuleId(rule.getId()).
                                    setMessage("Invalid configuration").
                                    setErrors("Action module not present: " + actionConfiguration.getModuleKey()).build());
                        }
                    }

                    // produce some reasonable audit logging
                    final String triggerMessage = String.format("%s resulted in %d items.", trigger.getAuditLog().getString(), Iterables.size(items));

                    // limit error messages to first MAX_ERRORS instead of getting all of them
                    final int lastErrorMessageIdx = Math.min(MAX_ERRORS, ruleErrors.getErrorMessages().size());
                    String errorMessages = StringUtils.join(ruleErrors.getErrorMessages().subList(0, lastErrorMessageIdx), ";");

                    // we need to get errors as well
                    final int lastErrorIdx = Math.min(MAX_ERRORS, ruleErrors.getErrors().size());
                    errorMessages += StringUtils.join(Lists.newArrayList(ruleErrors.getErrors().values()).subList(0, lastErrorIdx), ";");

                    if (ruleErrors.getErrorMessages().size() > MAX_ERRORS || ruleErrors.getErrors().size() > MAX_ERRORS)
                    {
                        // we had more errors than stored, so let's just make a note of that
                        errorMessages += " There were more errors, logging only first " + (lastErrorMessageIdx + lastErrorIdx);
                    }

                    context.getAuditLogService().addEntry(new AuditMessageBuilder().
                            setActor(rule.getActor()).
                            setRuleId(rule.getId()).
                            setMessage("Executed rule").
                            setTriggerMessage(triggerMessage).
                            setActionMessages(actionLogs).
                            setErrors(errorMessages).build());
                }
            }
            catch (Exception e)
            {
                log.error("Exception while executing the rule", e);
                context.getAuditLogService().addEntry(new AuditMessageBuilder().
                        setActor(rule.getActor()).
                        setRuleId(rule.getId()).
                        setMessage("Exception occurred").
                        setErrors(e.getClass().getCanonicalName()).build());
            }
        }
        else
        {
            log.error("No trigger with module key '" + rule.getTriggerConfiguration().getModuleKey() +
                    "' could be loaded for rule '" + rule.getName() + "'. Is the plugin providing this trigger still installed?");
            context.getAuditLogService().addEntry(new AuditMessageBuilder().
                    setActor(rule.getActor()).
                    setRuleId(rule.getId()).
                    setMessage("Invalid configuration").
                    setErrors("Trigger module not present: " + rule.getTriggerConfiguration().getModuleKey()).build());

        }
        return ruleErrors;
    }

    private ErrorCollection disableRuleAndReturn(final Rule rule)
    {
        ErrorCollection ruleErrors = new ErrorCollection();

        // This is a little bit tricky, what if the actor was deleted? We won't be able to stop the rule! But using this iso hardcoding "admin"
        final RuleService ruleService = context.getRuleService();
        final Either<ErrorCollection, RuleService.UpdateRuleStatusValidationResult> validationResult =
                ruleService.validateUpdateRuleStatus(rule.getActor(), rule.getId());
        if (validationResult.isLeft())
        {
            ruleErrors = validationResult.left().get();
            final String errors = StringUtils.join(Lists.newArrayList(ruleErrors.getErrors().values()), ";");
            context.getAuditLogService().addEntry(new AuditMessageBuilder().setMessage("Unable to disable rule").setRuleId(rule.getId()).
                    setErrors(errors).build());
            return ruleErrors;
        }
        else
        {
            RuleService.UpdateRuleStatusValidationResult result = validationResult.right().get();
            ruleService.updateRuleStatus(result, false);
            return ruleErrors;
        }
    }
}

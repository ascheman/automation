package com.atlassian.plugin.automation.scheduler;

import com.atlassian.plugin.automation.core.Rule;

/**
 * Provides way to limit the rule execution by applying e.g. thresholds
 */
public interface RuleExecutionLimiter
{
    /**
     *
     * @param rule
     * @param triggerString
     * @return Returns false if the rule cannot be executed (e.g. due to some conditions)
     */
    boolean canExecuteActions(Rule rule, String triggerString);
}

package com.atlassian.plugin.automation.scheduler;

import com.atlassian.plugin.automation.config.Settings;
import com.atlassian.plugin.automation.core.Rule;
import com.atlassian.plugin.automation.core.auditlog.AuditMessageBuilder;
import com.atlassian.plugin.automation.service.AuditLogService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.google.common.base.Supplier;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Limits the execution of the rules by checking how many times certain rule was executed within given time frame
 */
@Named
public class DefaultRuleExecutionLimiter implements RuleExecutionLimiter
{
    private static final Logger log = Logger.getLogger(DefaultRuleExecutionLimiter.class);
    private static final int MIN_TIME_BETWEEN_EXECUTIONS_MS = 500;
    private static final int MAX_ACCESS_COUNT = 20;

    private final Cache<Rule, AccessTracker> ruleExecutions;

    private final AuditLogService auditLogService;
    private final PluginSettings pluginSettings;

    @Inject
    public DefaultRuleExecutionLimiter(
            @ComponentImport final PluginSettingsFactory pluginSettingsFactory,
            final AuditLogService auditLogService)
    {
        this.pluginSettings = pluginSettingsFactory.createGlobalSettings();
        this.auditLogService = auditLogService;
        ruleExecutions = CacheBuilder.newBuilder().build(CacheLoader.from(new Supplier<AccessTracker>()
        {
            @Override
            public AccessTracker get()
            {
                return new AccessTracker();
            }
        }));
    }

    @Override
    public boolean canExecuteActions(final Rule rule, String triggerString)
    {
        final boolean isLimiterEnabled = Boolean.valueOf((String) pluginSettings.get(Settings.LIMITER_ENABLED));

        if (isLimiterEnabled)
        {
            AccessTracker accessTracker = ruleExecutions.getUnchecked(rule);
            final int accessCount = accessTracker.access();
            if (accessCount > MAX_ACCESS_COUNT)
            {
                final String message = String.format("Possible loop detected! Rule '%s' was executed more than '%d' times in last '%d' ms.",
                        rule.getName(), accessCount - 1, MIN_TIME_BETWEEN_EXECUTIONS_MS);
                log.error(message);
                auditLogService.addEntry(new AuditMessageBuilder().setMessage("Auto-disabling rule").
                        setErrors(message).setTriggerMessage(triggerString).setRuleId(rule.getId()).setActor(rule.getActor()).build());
                return false;
            }
        }
        return true;
    }

    static class AccessTracker
    {
        long lastTimestamp;
        int accessCount;

        AccessTracker()
        {
            this.lastTimestamp = System.currentTimeMillis();
            accessCount = 0;
        }

        int access()
        {
            final long currrentTime = System.currentTimeMillis();
            final long difference = Math.abs(currrentTime - lastTimestamp);
            lastTimestamp = currrentTime;
            if (difference < MIN_TIME_BETWEEN_EXECUTIONS_MS)
            {
                accessCount++;
            }
            else
            {
                accessCount = 1;
            }
            return accessCount;
        }
    }
}

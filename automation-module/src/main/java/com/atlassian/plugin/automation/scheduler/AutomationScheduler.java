package com.atlassian.plugin.automation.scheduler;

import com.atlassian.plugin.automation.core.Rule;
import com.atlassian.plugin.automation.service.RuleService;

/**
 * This interface provides unified way of entering tasks to automation queue
 */
public interface AutomationScheduler
{
    /**
     * Schedules given rule according to it's trigger
     * @param rule
     * @param ruleService
     */
    void scheduleRule(final Rule rule, RuleService ruleService);

    /**
     * Unschedule rule once it's removed
     * @param rule
     */
    void unscheduleRule(Rule rule);

    /**
     * Unschedules all rules.
     */
    void unscheduleAllRules();
}

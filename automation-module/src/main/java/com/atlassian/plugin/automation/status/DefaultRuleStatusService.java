package com.atlassian.plugin.automation.status;

import com.google.common.collect.Maps;

import java.util.concurrent.ConcurrentMap;
import javax.inject.Named;

@Named
public class DefaultRuleStatusService implements RuleStatusService
{
    private final ConcurrentMap<Integer, RuleStatus> ruleStatuses;

    public DefaultRuleStatusService()
    {
        this.ruleStatuses = Maps.newConcurrentMap();
    }

    @Override
    public void setRuleStatus(int ruleId, RuleStatus status)
    {
        ruleStatuses.put(ruleId, status);
    }

    @Override
    public RuleStatus getRuleStatus(int ruleId)
    {
        final RuleStatus ruleStatus = ruleStatuses.get(ruleId);
        if (ruleStatus != null)
        {
            return ruleStatus;
        }
        return RuleStatus.UNKNOWN;
    }
}

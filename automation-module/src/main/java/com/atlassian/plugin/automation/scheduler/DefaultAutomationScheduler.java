package com.atlassian.plugin.automation.scheduler;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.automation.core.CronTrigger;
import com.atlassian.plugin.automation.core.EventTrigger;
import com.atlassian.plugin.automation.core.Rule;
import com.atlassian.plugin.automation.core.Trigger;
import com.atlassian.plugin.automation.core.auditlog.AuditMessageBuilder;
import com.atlassian.plugin.automation.core.trigger.TriggerConfiguration;
import com.atlassian.plugin.automation.module.AutomationModuleManager;
import com.atlassian.plugin.automation.service.AuditLogService;
import com.atlassian.plugin.automation.service.RuleService;
import com.atlassian.plugin.automation.spi.CronScheduler;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.google.common.collect.Maps;
import org.apache.log4j.Logger;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.inject.Inject;
import javax.inject.Named;

@Named
public class DefaultAutomationScheduler implements AutomationScheduler
{
    private static final Logger log = Logger.getLogger(DefaultAutomationScheduler.class);

    public static final String JOB_KEY_RULE_CONTEXT = "KEY_RULE_CONTEXT";

    private final ConcurrentMap<Integer, HandlerIdentifier> handlers = new ConcurrentHashMap<Integer, HandlerIdentifier>();
    private final EventPublisher eventPublisher;
    private final CronScheduler cronScheduler;
    private final TransactionTemplate transactionTemplate;
    private final AutomationModuleManager automationModuleManager;
    private final RuleExecutionLimiter ruleExecutionLimiter;
    private final AuditLogService auditLogService;
    private final ThreadLocalExecutor threadLocalExecutor;
    private final ExecutorService eventExecutor = Executors.newSingleThreadExecutor();

    @Inject
    public DefaultAutomationScheduler(
            @ComponentImport final EventPublisher eventPublisher,
            @ComponentImport final CronScheduler cronScheduler,
            @ComponentImport final TransactionTemplate transactionTemplate,
            final ThreadLocalExecutor threadLocalExecutor,
            final AuditLogService auditLogService,
            final AutomationModuleManager automationModuleManager,
            final RuleExecutionLimiter ruleExecutionLimiter)
    {
        this.threadLocalExecutor = threadLocalExecutor;
        this.eventPublisher = eventPublisher;
        this.auditLogService = auditLogService;
        this.cronScheduler = cronScheduler;
        this.transactionTemplate = transactionTemplate;
        this.automationModuleManager = automationModuleManager;
        this.ruleExecutionLimiter = ruleExecutionLimiter;
    }

    @Override
    public synchronized void scheduleRule(final Rule rule, final RuleService ruleService)
    {
        if (!rule.isEnabled())
        {
            log.info("Trying to schedule disabled rule. Skipping rule: " + rule.getName());
            return;
        }

        if (handlers.containsKey(rule.getId()))
        {
            log.info("Trying to schedule already scheduled rule. Unscheduling rule: " + rule.getName());
            unscheduleRule(rule);
        }

        final TriggerConfiguration config = rule.getTriggerConfiguration();

        final Trigger trigger = automationModuleManager.getTrigger(config.getModuleKey());
        if (trigger != null)
        {
            trigger.init(config);
            final RuleCallableContext ruleContext = new RuleCallableContext(rule, threadLocalExecutor, auditLogService, transactionTemplate,
                    automationModuleManager, ruleExecutionLimiter, ruleService);
            if (trigger instanceof EventTrigger)
            {
                final String eventClassName = ((EventTrigger) trigger).getEventClassName();

                final AutomationEventHandler eventHandler = new DefaultAutomationEventHandler(ruleContext, eventExecutor, eventClassName);
                eventPublisher.register(eventHandler);
                handlers.putIfAbsent(rule.getId(), HandlerIdentifier.event(eventHandler));
            }
            else if (trigger instanceof CronTrigger)
            {
                final String jobKey = "AutomationRule-" + rule.getId();
                final Map<String, Object> params = Maps.newHashMap();
                params.put(JOB_KEY_RULE_CONTEXT, ruleContext);
                final String cronExpression = ((CronTrigger) trigger).getCronString();
                cronScheduler.scheduleJob(jobKey, CronScheduledTriggerJob.class, params, cronExpression);
                handlers.putIfAbsent(rule.getId(), HandlerIdentifier.cron(jobKey));
            }
            else
            {
                throw new IllegalStateException("Invalid Trigger type configured for rule '" + rule.getName() + "'");
            }
        }
        else
        {
            auditLogService.addEntry(new AuditMessageBuilder().setActor(rule.getActor()).
                    setRuleId(rule.getId()).
                    setMessage("Invalid configuration").
                    setErrors("Trigger module not present: " + rule.getTriggerConfiguration().getModuleKey()).build());
        }
    }

    @Override
    public synchronized void unscheduleRule(Rule rule)
    {
        final HandlerIdentifier handlerIdentifier = handlers.remove(rule.getId());
        if (handlerIdentifier != null)
        {
            log.info("Unregistering handler for job: " + rule.getName());
            handlerIdentifier.unschedule(eventPublisher, cronScheduler);
        }
    }

    @Override
    public void unscheduleAllRules()
    {
        for (Map.Entry<Integer, HandlerIdentifier> entry : handlers.entrySet())
        {
            log.info("Unscheduling rule ID: " + entry.getKey());
            entry.getValue().unschedule(eventPublisher, cronScheduler);
        }
        handlers.clear();
    }

    static class HandlerIdentifier
    {
        private final AutomationEventHandler eventHandler;
        private final String cronTrigger;

        public static HandlerIdentifier cron(String cronTrigger)
        {
            return new HandlerIdentifier(cronTrigger);
        }

        public static HandlerIdentifier event(AutomationEventHandler eventHandler)
        {
            return new HandlerIdentifier(eventHandler);
        }

        public void unschedule(EventPublisher publisher, CronScheduler scheduler)
        {
            try
            {
                if (cronTrigger != null)
                {
                    scheduler.unscheduleJob(cronTrigger);
                }
                else
                {
                    publisher.unregister(eventHandler);
                }
            }
            catch (RuntimeException e)
            {
                log.error("Unexpected error while un-scheduling rule.", e);
            }
        }

        private HandlerIdentifier(String cronTrigger)
        {
            this.cronTrigger = cronTrigger;
            this.eventHandler = null;
        }

        private HandlerIdentifier(AutomationEventHandler eventHandler)
        {
            this.cronTrigger = null;
            this.eventHandler = eventHandler;
        }
    }
}

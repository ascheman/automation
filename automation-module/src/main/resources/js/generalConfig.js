(function () {
    AJS.$(function () {
        AJS.$("#config-form").submit(function (e) {
            e.preventDefault();
            var $this = AJS.$(this);
            AJS.$(".throbber", $this).addClass('loading');
            var data = {isRateLimiterEnabled: AJS.$('#rate-limiter-enabled', $this).is(':checked')};
            AJS.$.ajax({
                url: $this.attr("action"),
                type: "PUT",
                contentType: "application/json",
                data: JSON.stringify(data),
                error: function (resp) {
                    alert(AJS.I18n.getText("automation.plugin.unknown.error"));
                },
                complete: function () {
                    AJS.$(".throbber", $this).removeClass('loading');
                }
            });
        });
    });
})();

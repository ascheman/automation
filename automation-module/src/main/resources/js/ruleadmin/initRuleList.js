(function () {
    AJS.$(function () {

        var ruleCollection = new Atlassian.Automation.RuleCollection();

        ruleCollection.fetch({
            reset: true,
            success: function () {
                //this will cause the ruleid to be picked up from the URL.
                Backbone.history.start();
            },
            error: function () {
                alert(AJS.I18n.getText("automation.plugin.error.loading.rules"));
            }
        });

        var selectionModel = new Atlassian.Automation.SelectionModel();

        var ruleRouter = new Atlassian.Automation.RuleRouter();
        ruleRouter.on("route:getRule", function (ruleId) {
            if (ruleId === undefined) {
                selectionModel.setSelection(ruleCollection.first());
            } else {
                var selectedModel = ruleCollection.get(ruleId)
                if(selectedModel !== undefined) {
                    selectionModel.setSelection(selectedModel);
                } else {
                    selectionModel.setSelection(ruleCollection.first());
                }
            }
        });

        selectionModel.on("change:selection", function () {
            var selectedModel = selectionModel.getSelection();
            if (selectedModel) {
                ruleRouter.navigate("rule/" + selectedModel.getId(), {trigger: false});
            }
        });

        var globalView = new Atlassian.Automation.GlobalRuleView({
            el: AJS.$("#rule-container"),
            model: ruleCollection,
            selectionModel: selectionModel
        });
    });
})();
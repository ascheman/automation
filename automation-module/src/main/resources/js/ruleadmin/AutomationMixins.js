AJS.namespace("Atlassian.Automation.Mixin.ValidationErrors");
AJS.namespace("Atlassian.Automation.Mixin.SetParams");

(function () {

    Atlassian.Automation.Mixin.SetParams = {
        setFormParams: function(rawParams, options) {
            var validParams = {};
            delete rawParams.moduleKey;

            //convert all single value params to an array since we
            //now support multi value params.
            _.each(rawParams, function(value, key) {
                var val = value;
                if(!AJS.$.isArray(value)) {
                    val = [value];
                }
                validParams[key] = val;
            });

            this.set({params:validParams}, options);
        }
    };

    Atlassian.Automation.Mixin.ValidationErrors = {
        addFormErrors: function (errorCollections, type) {
            //clear all errors
            this.$("form .error").remove();
            var $form = this.$("form");

            if (typeof errorCollections !== "undefined") {
                var errorCollection = errorCollections[type];
                if (typeof errorCollection !== "undefined") {
                    AJS.$.each(errorCollection.errors, function (field, message) {
                        AJS.$("[name=" + field + "]", $form).after(Atlassian.Templates.Automation.errorDiv({message: message}));
                    });
                }
            }
        }
    };
})();
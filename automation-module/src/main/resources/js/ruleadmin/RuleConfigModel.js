AJS.namespace("Atlassian.Automation.RuleConfigModel");

Atlassian.Automation.RuleConfigModel = Brace.Model.extend({
    namedAttributes: ["rule", "progress", "triggerModules", "actionModules", "currentUser", "errors", "copiedRule"],
    defaults: {
        rule: new Atlassian.Automation.RuleModel(),
        progress: 0,
        copiedRule: false
    },

    initialize: function () {
        this.get("rule").set("actor", this.get("currentUser"));
    },

    validateAndIncrementProgress: function (done) {
        var that = this;

        that.unset("errors");

        var data = this.get("rule").toJSON();
        //remove the status since we only need it client-side for display
        data.status = undefined;

        AJS.$.ajax({
            url: AJS.contextPath() + "/rest/automation/1.0/rule/validate/" + this.get("progress"),
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(data),
            success: function (resp) {
                var rule = that.get("rule");
                //update views used to render preview screen with what came back from the server!
                rule.setTriggerView(resp.triggerView);
                rule.setActionViews(resp.actionViews);

                that.set("progress", that.get("progress") + 1);
            },
            error: function (resp) {
                if (resp && resp.status === 400 && resp.responseText) {
                    var errors = JSON.parse(resp.responseText);
                    that.set("errors", errors);
                } else {
                    alert(AJS.I18n.getText("automation.plugin.unknown.error.reload"))
                }
            },
            complete: function () {
                done();
            }
        });
    }
});

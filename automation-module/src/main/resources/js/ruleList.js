(function () {
    AJS.$(function () {
        AJS.$("#rule-list .delete-lnk").click(function (e) {
            e.preventDefault();
            if (confirm(AJS.I18n.getText("automation.plugin.delete.confirm"))) {
                AJS.$.ajax({
                    url: AJS.$(this).attr("href"),
                    type: "DELETE",
                    contentType: "application/json",
                    success: function (resp) {
                        window.location.reload();
                    },
                    error: function (resp) {
                        alert(AJS.I18n.getText("automation.plugin.unknown.error"));
                    }
                });
            }
        });

        AJS.$("#rule-list .disable-rule-lnk").click(function (e) {
            e.preventDefault();
            if (confirm(AJS.I18n.getText("automation.plugin.disable.confirm"))) {
                AJS.$.ajax({
                    url: AJS.$(this).attr("href"),
                    type: "PUT",
                    contentType: "application/json",
                    success: function (resp) {
                        window.location.reload();
                    },
                    error: function (resp) {
                        alert(AJS.I18n.getText("automation.plugin.unknown.error"));
                    }
                });
            }
        });

        AJS.$("#rule-list .enable-rule-lnk").click(function (e) {
            e.preventDefault();
            if (confirm(AJS.I18n.getText("automation.plugin.enable.confirm"))) {
                AJS.$.ajax({
                    url: AJS.$(this).attr("href"),
                    type: "PUT",
                    contentType: "application/json",
                    success: function (resp) {
                        window.location.reload();
                    },
                    error: function (resp) {
                        alert(AJS.I18n.getText("automation.plugin.unknown.error"));
                    }
                });
            }
        });
    });
})();

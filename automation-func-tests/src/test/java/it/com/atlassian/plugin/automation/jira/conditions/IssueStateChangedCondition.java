package it.com.atlassian.plugin.automation.jira.conditions;

import com.atlassian.jira.testkit.client.IssuesControl;
import com.atlassian.jira.testkit.client.restclient.Status;
import com.atlassian.pageobjects.elements.query.AbstractTimedCondition;

public class IssueStateChangedCondition extends AbstractTimedCondition
{
    private static final int MAX_TIMEOUT = 15000;
    private static final int POLLING_INTERVAL = 500;
    private final IssuesControl issuesControl;
    private final String issueKey;
    private final String statusName;

    public IssueStateChangedCondition(final IssuesControl issuesControl, String issueKey, String statusName)
    {
        super(MAX_TIMEOUT, POLLING_INTERVAL);
        this.issuesControl = issuesControl;
        this.issueKey = issueKey;
        this.statusName = statusName;
    }

    @Override
    protected Boolean currentValue()
    {
        final Status status = issuesControl.getIssue(issueKey).fields.status;
        return status.name().equals(statusName);

    }
}

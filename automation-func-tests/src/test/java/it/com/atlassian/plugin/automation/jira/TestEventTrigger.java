package it.com.atlassian.plugin.automation.jira;

import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.client.IssuesControl;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.jira.tests.TestBase;
import com.atlassian.plugin.automation.page.ActionsForm;
import com.atlassian.plugin.automation.page.AdminPage;
import com.atlassian.plugin.automation.page.CleanupRule;
import com.atlassian.plugin.automation.page.ConfigRuleForm;
import com.atlassian.plugin.automation.page.TriggerPage;
import com.atlassian.plugin.automation.page.action.CommentIssueActionForm;
import com.atlassian.plugin.automation.page.trigger.IssueEventTriggerForm;
import it.com.atlassian.plugin.automation.jira.conditions.IssueCommentedCondition;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.junit.Assert.assertEquals;

public class TestEventTrigger extends TestBase
{
    @Rule
    public CleanupRule cleanup = new CleanupRule(jira());

    @Before
    public void setup()
    {
        backdoor().dataImport().restoreDataFromResource("jira-automation-data.zip");
        backdoor().project().addProject("Another Test", "TESTTWO", "admin");
    }

    @Test
    public void testEventTriggerWithJQL()
    {
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);

        assertEquals("Some rules are present", 0, adminPage.getRulesCount());
        final ConfigRuleForm configRuleForm = adminPage.addRuleForm();
        final TriggerPage triggerPage = configRuleForm.ruleName("test").
                enabled(true).
                next();
        triggerPage.selectTrigger(IssueEventTriggerForm.class).
                setEvents("Issue Created").
                setJql("project = TEST");
        final ActionsForm actionsForm = triggerPage.next();

        final CommentIssueActionForm actionForm = actionsForm.setInitialAction(CommentIssueActionForm.class);
        actionForm.comment("test");
        actionsForm.next().save();

        final IssueCreateResponse issueInProject1 = backdoor().issues().createIssue("TESTTWO", "Issue shouldn't get comments", null);
        String issue1Key = issueInProject1.key;

        final IssueCreateResponse issueInProject2 = backdoor().issues().createIssue("TEST", "Issue should get comments", null);
        String issue2Key = issueInProject2.key;

        waitUntilTrue(new IssueCommentedCondition(jira().backdoor().issues(), issue2Key, 1));

        Issue issue1 = jira().backdoor().issues().getIssue(issue1Key);
        assertEquals("Expected 0 comments", 0, issue1.getComments().size());

        Issue issue2 = jira().backdoor().issues().getIssue(issue2Key);
        assertEquals("Expected 1 comments", 1, issue2.getComments().size());
    }

    @Test
    public void testMultipleEventTrigger()
    {
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);

        assertEquals("Some rules are present", 0, adminPage.getRulesCount());
        final ConfigRuleForm configRuleForm = adminPage.addRuleForm();
        final TriggerPage triggerPage = configRuleForm.ruleName("test").
                enabled(true).
                next();
        triggerPage.selectTrigger(IssueEventTriggerForm.class).
                setEvents("Issue Created", "Issue Closed");
        ActionsForm actionsForm = triggerPage.next();

        final CommentIssueActionForm actionForm = actionsForm.setInitialAction(CommentIssueActionForm.class);
        actionForm.comment("test");
        actionsForm.next().save();

        final IssueCreateResponse issue = backdoor().issues().createIssue("TEST", "Issue should get comments", null);
        String issueKey = issue.key;

        waitUntilTrue(new IssueCommentedCondition(jira().backdoor().issues(), issueKey, 1));

        Issue issueObject = jira().backdoor().issues().getIssue(issueKey);
        assertEquals("Expected 1 comments", 1, issueObject.getComments().size());

        jira().goToViewIssue(issueKey).closeIssue();

        waitUntilTrue(new IssueCommentedCondition(jira().backdoor().issues(), issueKey, 2));

        issueObject = jira().backdoor().issues().getIssue(issueKey);
        assertEquals("Expected 2 comments", 2, issueObject.getComments().size());
    }

    @Test
    public void testEventTriggerLimitUser()
    {
        backdoor().usersAndGroups().addUser("fred");
        backdoor().usersAndGroups().addUserToGroup("fred", "jira-users");
        backdoor().usersAndGroups().addUserToGroup("fred", "jira-developers");

        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);

        assertEquals("Some rules are present", 0, adminPage.getRulesCount());
        final ConfigRuleForm configRuleForm = adminPage.addRuleForm();
        final TriggerPage triggerPage = configRuleForm.ruleName("test").
                enabled(true).
                next();
        triggerPage.selectTrigger(IssueEventTriggerForm.class).setEvents("Issue Updated").toggleRestrictUsers().toggleAssigneeOnly();
        final ActionsForm actionsForm = triggerPage.next();

        final CommentIssueActionForm actionForm = actionsForm.setInitialAction(CommentIssueActionForm.class);
        actionForm.comment("Issue Updated!");
        actionsForm.next().save();

        final IssueCreateResponse issue = backdoor().issues().createIssue("TEST", "Issue should get comments from fred only!", "fred");
        waitUntilTrue(new IssueCommentedCondition(jira().backdoor().issues(), issue.key, 0));

        //now update the issue again as admin. This should have no effect still 'cause we're logged in as admin!
        backdoor().issues().setDescription(issue.key, "Edited!");
        waitUntilTrue(new IssueCommentedCondition(jira().backdoor().issues(), issue.key, 0));

        //lets do it as fred. This should add a comment!
        final IssuesControl issues = backdoor().issues();
        issues.loginAs("fred").setDescription(issue.key, "Edited by Fred!");
        waitUntilTrue(new IssueCommentedCondition(jira().backdoor().issues(), issue.key, 1));
    }
}

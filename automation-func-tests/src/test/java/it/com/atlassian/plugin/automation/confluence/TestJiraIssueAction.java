package it.com.atlassian.plugin.automation.confluence;

import com.atlassian.applinks.api.application.jira.JiraProjectEntityType;
import com.atlassian.confluence.it.User;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.testkit.client.restclient.SearchClient;
import com.atlassian.jira.testkit.client.restclient.SearchRequest;
import com.atlassian.jira.testkit.client.restclient.SearchResult;
import com.atlassian.jira.tests.FuncTestHelper;
import com.atlassian.jira.tests.JiraTestedProductHelper;
import com.atlassian.pageobjects.elements.query.AbstractTimedCondition;
import com.atlassian.plugin.automation.page.ActionsForm;
import com.atlassian.plugin.automation.page.AdminPage;
import com.atlassian.plugin.automation.page.CleanupRule;
import com.atlassian.plugin.automation.page.ConfigRuleForm;
import com.atlassian.plugin.automation.page.TriggerPage;
import com.atlassian.plugin.automation.page.action.CreateJiraIssueActionForm;
import com.atlassian.plugin.automation.page.trigger.ConfluenceSearchTriggerForm;
import com.atlassian.webdriver.applinks.component.AddEntityLinkSection;
import com.atlassian.webdriver.applinks.page.ConfigureEntityLinksPage;
import com.atlassian.webdriver.applinks.page.ListApplicationLinkPage;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.After;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.List;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static junit.framework.Assert.assertEquals;

public class TestJiraIssueAction extends ConfluenceTestBase
{
    private static final String HSP_JQL = "project = HSP";
    @org.junit.Rule
    public CleanupRule cleanup = new CleanupRule(confluence);

    @ClassRule
    public static FuncTestHelper funcTestHelper = new FuncTestHelper();

    @ClassRule
    public static JiraTestedProductHelper productHelper = new JiraTestedProductHelper();

    final JiraTestedProduct jira = productHelper.jira();

    @After
    public void cleanup()
    {
        // Remove the applink once done!
        jira.goTo(ListApplicationLinkPage.class).deleteApplicationLink(localEnvironment.getBaseUrl()).deleteTwoWayLink(true);
    }

//    @Test
//    public void testMultiple() throws Exception
//    {
//        for(int i = 0;i<10;i++)
//        {
//            testCreateJiraIssue();
//            cleanup();
//            confluence.logOut();
//            start();
//            System.out.println("Run " + i + " success...");
//        }
//    }

    /*@Test
    public void testCreateJiraIssue() throws InterruptedException
    {
        jira.backdoor().restoreBlankInstance();

        final ListApplicationLinkPage applinksPage = jira.gotoLoginPage().loginAsSysAdmin(ListApplicationLinkPage.class).addApplicationLink()
                .setApplicationUrl(confluence.getProductInstance().getBaseUrl())
                .nextExpectsUalStep2()
                .configureTwoWayLink("admin", "admin", jira.getProductInstance().getBaseUrl())
                .selectSameUserBaseAndTrustedLink();

        waitUntil(applinksPage.getApplicationLinksTimed(), new ApplinksRowMatcher(confluence.getProductInstance().getBaseUrl()));

        //applinks page objects are flaky!!
        Thread.sleep(2000);

        ConfigureEntityLinksPage hspEntityLinks = jira.goTo(ConfigureEntityLinksPage.class, JiraProjectEntityType.class, "HSP");
        assertEquals("No links just yet", 0, hspEntityLinks.getEntityLinkCount());
        final AddEntityLinkSection addLinkDialog = hspEntityLinks.addLink("Confluence");

        //applinks page objects are flaky!!
        Thread.sleep(2000);
        addLinkDialog.submit();

        waitUntilTrue(hspEntityLinks.hasRow("remote-key", "TST"));

        final SearchResult results = jira.backdoor().search().getSearch(new SearchRequest().jql(HSP_JQL));
        assertEquals("No issues created yet", 0, results.total.intValue());

        AdminPage automationAdmin = confluence.gotoLoginPage().login(User.ADMIN, AdminPage.class);
        assertEquals("No rules configured yet", 0, automationAdmin.getRules().size());
        final ConfigRuleForm configRuleForm = automationAdmin.addRuleForm();
        final TriggerPage triggerPage = configRuleForm.
                ruleName("Create JIRA Issues").
                enabled(true).next();
        triggerPage.selectTrigger(ConfluenceSearchTriggerForm.class).setCron("0/10 * * * * ?").setQuery("test").setSpaceKeys("TST");
        final ActionsForm actionsForm = triggerPage.next();


        final CreateJiraIssueActionForm actionForm = actionsForm.setInitialAction(CreateJiraIssueActionForm.class);
        actionForm.noUpdatePeriod(1);
        automationAdmin = actionsForm.next().save();

        assertEquals("Got a rule!", 1, automationAdmin.getRules().size());

        waitUntilTrue(new IssuesCreatedCondition(jira.backdoor().search(), HSP_JQL, 2));
    }*/

    public static class ApplinksRowMatcher extends BaseMatcher<List<ListApplicationLinkPage.ApplicationLinkEntryRow>>
    {
        private final String baseUrl;

        public ApplinksRowMatcher(final String baseUrl)
        {
            this.baseUrl = baseUrl;
        }

        @Override
        public void describeTo(final Description description)
        {
            description.appendText("Matching applinks baseurl");
        }

        @Override
        public boolean matches(final Object o)
        {
            List<ListApplicationLinkPage.ApplicationLinkEntryRow> rows = (List<ListApplicationLinkPage.ApplicationLinkEntryRow>) o;
            for (ListApplicationLinkPage.ApplicationLinkEntryRow row : rows)
            {
                if (row.getApplicationUrl().equals(baseUrl))
                {
                    return true;
                }
            }

            return false;
        }
    }

    public static class IssuesCreatedCondition extends AbstractTimedCondition
    {
        private static final int MAX_TIMEOUT = 30000;
        private static final int POLLING_INTERVAL = 1000;
        private final SearchClient searchClient;
        private final String jql;
        private final int expectedIssues;

        public IssuesCreatedCondition(final SearchClient searchClient, String jql, int expectedIssues)
        {
            super(MAX_TIMEOUT, POLLING_INTERVAL);

            this.searchClient = searchClient;
            this.jql = jql;
            this.expectedIssues = expectedIssues;
        }

        @Override
        protected Boolean currentValue()
        {
            final SearchResult result = this.searchClient.getSearch(new SearchRequest().jql(this.jql));
            return result.total == this.expectedIssues;
        }
    }
}

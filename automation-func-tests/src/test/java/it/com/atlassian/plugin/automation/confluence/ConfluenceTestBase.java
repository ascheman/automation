package it.com.atlassian.plugin.automation.confluence;

import com.atlassian.confluence.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.webdriver.AbstractWebDriverTest;
import com.atlassian.plugin.automation.page.util.LocalEnvironmentProductInstance;

/**
 * Confluence test base
 */
public abstract class ConfluenceTestBase extends AbstractWebDriverTest
{
    static
    {
        if (System.getProperty("confluence.version") == null)
        {
            System.setProperty("confluence.version", "5.1.1");
        }
    }

    //needs to be static so it runs before AbstractWebDriverTest, since it sets up some System properties used for the RPC client.
    protected static final LocalEnvironmentProductInstance localEnvironment = new LocalEnvironmentProductInstance();

    protected final ConfluenceTestedProduct confluence = new ConfluenceTestedProduct(null, localEnvironment);

}

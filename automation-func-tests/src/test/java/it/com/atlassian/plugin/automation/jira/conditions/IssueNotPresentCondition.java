package it.com.atlassian.plugin.automation.jira.conditions;

import com.atlassian.jira.testkit.client.restclient.SearchClient;
import com.atlassian.jira.testkit.client.restclient.SearchRequest;
import com.atlassian.pageobjects.elements.query.AbstractTimedCondition;

/**
 * @author: mkonecny
 */
public class IssueNotPresentCondition extends AbstractTimedCondition
{
    private static final int MAX_TIMEOUT = 25000;
    private static final int POLLING_INTERVAL = 500;
    private final SearchClient searchClient;
    private final SearchRequest searchRequest;


    public IssueNotPresentCondition(SearchClient searchClient, String issueKey)
    {
        super(MAX_TIMEOUT, POLLING_INTERVAL);
        this.searchClient = searchClient;

        searchRequest = new SearchRequest();
        searchRequest.jql("issueKey = " + issueKey);
    }

    @Override
    protected Boolean currentValue()
    {

        final int statusCode = searchClient.getSearchResponse(searchRequest).statusCode;
        return statusCode >= 400 && statusCode < 500;
    }
}

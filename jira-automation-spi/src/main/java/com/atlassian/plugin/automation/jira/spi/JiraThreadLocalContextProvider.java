package com.atlassian.plugin.automation.jira.spi;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.thread.JiraThreadLocalUtils;
import com.atlassian.plugin.automation.spi.ThreadLocalContextProvider;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Local thread context provider for JIRA
 */
@Named
@ExportAsService
public class JiraThreadLocalContextProvider implements ThreadLocalContextProvider
{
    private static final Logger log = Logger.getLogger(JiraThreadLocalContextProvider.class);
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final UserUtil userUtil;

    @Inject
    public JiraThreadLocalContextProvider(
            @ComponentImport JiraAuthenticationContext jiraAuthenticationContext,
            @ComponentImport UserUtil userUtil)
    {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.userUtil = userUtil;
    }

    @Override
    public void preCall(String actor)
    {
        JiraThreadLocalUtils.preCall();
        jiraAuthenticationContext.setLoggedInUser(userUtil.getUser(actor));
    }

    @Override
    public void postCall(String actor)
    {
        jiraAuthenticationContext.setLoggedInUser((ApplicationUser)null);
        JiraThreadLocalUtils.postCall(log, new JiraThreadLocalUtils.ProblemDeterminationCallback()
        {
            @Override
            public void onOpenTransaction()
            {
                log.error("A database connection was left open.");
            }
        });
    }
}

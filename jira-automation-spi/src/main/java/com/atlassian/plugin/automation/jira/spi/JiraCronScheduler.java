package com.atlassian.plugin.automation.jira.spi;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.plugin.automation.spi.CronScheduler;
import com.atlassian.plugin.automation.spi.QuartzPluginJob;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.scheduling.PluginJob;
import org.apache.log4j.Logger;
import org.quartz.*;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Map;

import javax.inject.Named;

import static java.lang.String.format;
import static org.quartz.Scheduler.DEFAULT_GROUP;

@Named
@ExportAsService
public class JiraCronScheduler implements CronScheduler
{
    private static final Logger log = Logger.getLogger(JiraCronScheduler.class);

    @Override
    public synchronized void scheduleJob(final String jobKey, final Class<? extends PluginJob> jobClass, final Map<String, Object> jobDataMap, final String cronExpression)
    {
        final Scheduler scheduler = ComponentAccessor.getComponentOfType(Scheduler.class);

        try
        {
            unscheduleJob(jobKey);

            final Trigger cronTrigger = new CronTrigger(jobKey, DEFAULT_GROUP, cronExpression);
            final JobDetail jobDetail = new JobDetail(jobKey, DEFAULT_GROUP, QuartzPluginJob.class, false, true, false);

            if(Arrays.asList(scheduler.getJobNames(DEFAULT_GROUP)).contains(jobKey))
            {
                log.info(String.format("Job with name '%s' already scheduled. Unscheduling first...", jobKey));
                unscheduleJob(jobKey);
            }

            final JobDataMap jobDetailMap = new JobDataMap() {
                /**
                 * This is possibly my worst hack ever but it's here so that JIRA's internal scheduler implementation
                 * doesn't complain about not being able to store job detail. We don't really care since we unschedule/reschedule these jobs anyway on startup.
                 *
                 * @return Always true so that JIRA thinks this puppy is empty!
                 */
                @Override
                public boolean isEmpty()
                {
                    return true;
                }
            };
            jobDetailMap.put(QuartzPluginJob.JOB_CLASS_KEY, jobClass);
            jobDetailMap.put(QuartzPluginJob.JOB_DATA_MAP_KEY, jobDataMap);
            jobDetail.setJobDataMap(jobDetailMap);

            scheduler.scheduleJob(jobDetail, cronTrigger);
        }
        catch (ParseException e)
        {
            log.error(format("Error scheduling job '%s' due to invalid cronExpression '%s'.", jobKey, cronExpression), e);
        }
        catch (SchedulerException e)
        {
            log.error(format("Error scheduling job '%s'.", jobKey), e);
        }
    }

    @Override
    public synchronized void unscheduleJob(final String jobKey)
    {
        try
        {
            final Scheduler scheduler = ComponentAccessor.getComponentOfType(Scheduler.class);

            scheduler.unscheduleJob(jobKey, DEFAULT_GROUP);
            scheduler.deleteJob(jobKey, DEFAULT_GROUP);
        }
        catch (final SchedulerException e)
        {
            throw new IllegalArgumentException(format("Error unscheduling job '%s'.", jobKey), e);
        }
    }
}

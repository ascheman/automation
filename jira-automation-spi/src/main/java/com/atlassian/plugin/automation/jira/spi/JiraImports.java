package com.atlassian.plugin.automation.jira.spi;

import com.atlassian.jira.security.PermissionManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Transitive imports
 */
@Named
public class JiraImports
{
    private final PermissionManager permissionManager;

    @Inject
    public JiraImports(
            @ComponentImport PermissionManager permissionManager
    )
    {
        this.permissionManager = permissionManager;
    }
}

package com.atlassian.plugin.automation.confluence.spi;

import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.plugin.automation.spi.ThreadLocalContextProvider;

/**
 */
public class ConfluenceThreadLocalContextProvider implements ThreadLocalContextProvider
{
    private final UserAccessor userAccessor;

    public ConfluenceThreadLocalContextProvider(final UserAccessor userAccessor)
    {
        this.userAccessor = userAccessor;
    }

    @Override
    public void preCall(final String actor)
    {
        AuthenticatedUserThreadLocal.setUser(userAccessor.getUser(actor));
    }

    @Override
    public void postCall(final String actor)
    {
        AuthenticatedUserThreadLocal.setUser(null);
    }
}

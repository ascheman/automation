package com.atlassian.plugin.automation.util;

import org.apache.commons.lang.StringUtils;
import org.quartz.CronTrigger;
import org.quartz.impl.calendar.BaseCalendar;

import java.text.ParseException;

public class CronExpressionValidator
{
    public static boolean isValidExpression(final String expression)
    {
        try
        {
            if (StringUtils.isBlank(expression))
            {
                return false;
            }
            final CronTrigger cronTrigger = new CronTrigger("test", "TEST-GROUP", expression);

            // Parsing the expression is not enough, we need to calculate the first fire time in order to get possible exception (yeck!)
            cronTrigger.computeFirstFireTime(new BaseCalendar());
            cronTrigger.getNextFireTime();
            return true;
        }
        catch (ParseException e)
        {
            return false;
        }
        catch (UnsupportedOperationException e)
        {
            return false;
        }
    }
}

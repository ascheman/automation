package com.atlassian.plugin.automation.service;

import com.atlassian.fugue.Either;
import com.atlassian.plugin.automation.core.auditlog.AuditMessage;
import com.atlassian.plugin.automation.util.ErrorCollection;

/**
 * Provides access to audit log
 */
public interface AuditLogService
{
    /**
     * Returns count of all audit log messages available.
     * @param user authentication user
     * @return
     */
    int getEntriesCount(String user);

    /**
     * Returns all entries from the audit log
     *
     * @param user authentication user
     * @param maxResults
     * @param startAt index of the first result (ordered by date desc)
     * @return
     */
    Either<ErrorCollection, Iterable<AuditMessage>> getAllEntries(String user, int startAt, int maxResults);

    /**
     * Adds given entry to the audit log
     *
     * @param auditMessage
     */
    void addEntry(AuditMessage auditMessage);

    /**
     * Truncates the log so only new entries are present
     */
    void truncateLog();
}

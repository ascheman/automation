package com.atlassian.plugin.automation.core;

/**
 * Represents triggers scheduled to execute at certain times.
 */
public interface CronTrigger<T> extends Trigger<T>
{
    /**
     * @return The CRON string requried to schedule a CronTrigger
     */
    String getCronString();
}

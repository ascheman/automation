package com.atlassian.plugin.automation.core.action;

import com.atlassian.plugin.automation.core.AutomationConfiguration;

/**
 * Marker interface for action configuration
 */
public interface ActionConfiguration extends AutomationConfiguration
{
}

package com.atlassian.plugin.automation.core.auditlog;

import com.google.common.collect.Lists;

import java.util.Date;

public class AuditMessageBuilder
{
    private Date timestamp = new Date();
    private String actor = null;
    private int ruleId;
    private String message;
    private String triggerMessage = "";
    private Iterable<String> actionMessages = Lists.<String>newArrayList();
    private String errors = "";

    public AuditMessageBuilder setTimestamp(final Date timestamp)
    {
        this.timestamp = timestamp;
        return this;
    }

    public AuditMessageBuilder setActor(final String actor)
    {
        this.actor = actor;
        return this;
    }

    public AuditMessageBuilder setRuleId(final int ruleId)
    {
        this.ruleId = ruleId;
        return this;
    }

    public AuditMessageBuilder setMessage(final String message)
    {
        this.message = message;
        return this;
    }

    public AuditMessageBuilder setTriggerMessage(final String triggerMessage)
    {
        this.triggerMessage = triggerMessage;
        return this;
    }

    public AuditMessageBuilder setActionMessages(final Iterable<String> actionMessages)
    {
        this.actionMessages = Lists.newArrayList(actionMessages);
        return this;
    }

    public AuditMessageBuilder setErrors(final String errors)
    {
        this.errors = errors;
        return this;
    }

    public DefaultAuditMessage build()
    {
        return new DefaultAuditMessage(timestamp, actor, ruleId, message, triggerMessage, actionMessages, errors);
    }
}
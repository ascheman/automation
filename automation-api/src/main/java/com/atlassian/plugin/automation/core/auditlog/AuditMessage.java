package com.atlassian.plugin.automation.core.auditlog;

import java.util.Date;

/**
 * This represents one single line in the audit log
 */
public interface AuditMessage
{
    /**
     * Timestamp of the message
     */
    Date getTimestamp();

    /**
     * Actor of the rule
     */
    String getActor();

    /**
     * Id of the rule
     */
    int getRuleId();

    /**
     * Returns the message to be shown
     */
    String getMessage();

    /**
     * @return returns a trigger specific message {@link com.atlassian.plugin.automation.core.Trigger#getAuditLog()}
     */
    String getTriggerMessage();

    /**
     * @return returns action specific messages {@link com.atlassian.plugin.automation.core.Action#getAuditLog()}
     */
    Iterable<String> getActionMessages();

    /**
     * The errors that have occurred
     */
    String getErrors();
}

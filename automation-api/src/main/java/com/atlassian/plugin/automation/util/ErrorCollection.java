package com.atlassian.plugin.automation.util;

import com.google.common.collect.Iterables;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;
import static javax.ws.rs.core.Response.Status;

/**
 * Represents validation errors
 */
public class ErrorCollection
{
    @JsonProperty
    private final List<String> errorMessages = newArrayList();

    @JsonProperty
    private final Map<String, String> errors = newHashMap();

    @JsonIgnore
    private final Set<Reason> reasons = newHashSet();


    public void addErrorMessage(String message)
    {
        errorMessages.add(message);
    }

    public void addError(String key, String message)
    {
        errors.put(key, message);
    }

    public List<String> getErrorMessages()
    {
        return errorMessages;
    }

    public void addErrorMessage(final String msg, final Reason reason)
    {
        errorMessages.add(msg);
        this.reasons.add(reason);
    }

    public Map<String, String> getErrors()
    {
        return errors;
    }

    public boolean hasAnyErrors()
    {
        return !errors.isEmpty() || !errorMessages.isEmpty();

    }

    public void addErrorCollection(final ErrorCollection errorCollection)
    {
        Iterables.addAll(errorMessages, errorCollection.getErrorMessages());
        errors.putAll(errorCollection.getErrors());
        this.reasons.addAll(errorCollection.getReasons());
    }

    public void addReason(final Reason reason)
    {
        this.reasons.add(reason);
    }

    public Set<Reason> getReasons()
    {
        return reasons;
    }

    public void addErrorMessages(final Set<String> errorMessages)
    {
        this.errorMessages.addAll(errorMessages);
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).
                append("errorMessages", errorMessages).
                append("errors", errors).
                append("reasons", reasons).
                toString();
    }

    public static enum Reason
    {
        /**
         * That which you are seeking is not here.
         */
        NOT_FOUND(Status.NOT_FOUND.getStatusCode()),

        /**
         * The user is not logged in.
         */
        NOT_LOGGED_IN(Status.UNAUTHORIZED.getStatusCode()),

        /**
         * Not allowed to perform function.
         */
        FORBIDDEN(Status.FORBIDDEN.getStatusCode()),

        /**
         * Data validation failed.
         */
        VALIDATION_FAILED(Status.BAD_REQUEST.getStatusCode()),

        /**
         * We are all broken.
         */
        SERVER_ERROR(Status.INTERNAL_SERVER_ERROR.getStatusCode());

        /**
         * The HTTP status code that is used to report this Reason.
         */
        private final int httpStatusCode;

        private Reason(int httpStatusCode)
        {
            this.httpStatusCode = httpStatusCode;
        }

        public int getHttpStatusCode()
        {
            return httpStatusCode;
        }

        public static Reason getWorstReason(final Collection<Reason> reasons)
        {
            if (reasons.contains(NOT_LOGGED_IN)) { return NOT_LOGGED_IN; }
            if (reasons.contains(FORBIDDEN)) { return FORBIDDEN; }
            if (reasons.contains(NOT_FOUND)) { return NOT_FOUND; }
            if (reasons.contains(SERVER_ERROR)) { return SERVER_ERROR; }
            if (reasons.contains(VALIDATION_FAILED)) { return VALIDATION_FAILED; }
            return null;
        }
    }
}

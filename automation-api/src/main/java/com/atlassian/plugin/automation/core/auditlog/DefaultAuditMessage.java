package com.atlassian.plugin.automation.core.auditlog;

import java.util.Date;

public class DefaultAuditMessage implements AuditMessage
{
    private final Date timestamp;
    private final String actor;
    private final int ruleId;
    private final String message;
    private final String triggerMessage;
    private final Iterable<String> actionMessages;
    private final String errors;

    DefaultAuditMessage(final Date timestamp, final String actor,
            final int ruleId, final String message, final String triggerMessage, final Iterable<String> actionMessages, final String errors)
    {
        this.timestamp = timestamp;
        this.actor = actor;
        this.ruleId = ruleId;
        this.message = message;
        this.triggerMessage = triggerMessage;
        this.actionMessages = actionMessages;
        this.errors = errors;
    }

    @Override
    public Date getTimestamp()
    {
        return timestamp;
    }

    @Override
    public String getActor()
    {
        return actor;
    }

    @Override
    public int getRuleId()
    {
        return ruleId;
    }

    @Override
    public String getMessage()
    {
        return message;
    }

    @Override
    public String getTriggerMessage()
    {
        return triggerMessage;
    }

    @Override
    public Iterable<String> getActionMessages()
    {
        return actionMessages;
    }

    @Override
    public String getErrors()
    {
        return errors;
    }
}

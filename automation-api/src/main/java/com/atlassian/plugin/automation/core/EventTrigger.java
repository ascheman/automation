package com.atlassian.plugin.automation.core;

/**
 * Represents a Trigger that's scheduled as the result of an incoming event
 */
public interface EventTrigger<T> extends Trigger<T>
{
    /**
     * @return The fully qualified classname of the event to listen for.
     */
    String getEventClassName();
}

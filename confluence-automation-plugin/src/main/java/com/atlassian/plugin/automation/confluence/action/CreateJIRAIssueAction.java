package com.atlassian.plugin.automation.confluence.action;

import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.EntityLink;
import com.atlassian.applinks.api.EntityLinkService;
import com.atlassian.applinks.api.application.jira.JiraProjectEntityType;
import com.atlassian.bonnie.Searchable;
import com.atlassian.confluence.core.persistence.AnyTypeDao;
import com.atlassian.confluence.labels.service.LabelsService;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.plugin.automation.core.Action;
import com.atlassian.plugin.automation.core.action.ActionConfiguration;
import com.atlassian.plugin.automation.core.auditlog.AuditString;
import com.atlassian.plugin.automation.core.auditlog.DefaultAuditString;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.plugin.automation.util.ParameterUtil;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.user.User;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.plugin.automation.util.ParameterUtil.singleValue;
import static java.lang.String.format;

/**
 * Creates JIRA issues using applinks for content that needs updating.
 * <p/>
 * Probably not the smartest implementation right now since its very chatty (request for each item found in search) but
 * that can be optimised later...
 */
public class CreateJIRAIssueAction implements Action<SearchResult>
{
    private static final String UPDATE_REQUESTED_LABEL = "updaterequested";
    private static final String RESOURCE_KEY = "com.atlassian.plugin.automation.confluence-automation-plugin:confluence-config-resources";
    private static final Logger log = Logger.getLogger(CreateJIRAIssueAction.class);
    private static final String NO_UPDATE_PERIOD_KEY = "noUpdatePeriod";
    private static final String ISSUE_TYPE_ID_KEY = "issueTypeId";

    private final EntityLinkService entityLinkService;
    private final SpaceManager spaceManager;
    private final ApplicationProperties applicationProperties;
    private final UserAccessor userAccessor;
    private final LabelsService labelsService;
    private final AnyTypeDao anyTypeDao;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private long noUpdatePeriod;
    private String issueTypeId;
    private int issuesCreated = 0;

    public CreateJIRAIssueAction(final EntityLinkService entityLinkService, final SpaceManager spaceManager,
            final ApplicationProperties applicationProperties, final UserAccessor userAccessor,
            final LabelsService labelsService, final AnyTypeDao anyTypeDao,
            final SoyTemplateRenderer soyTemplateRenderer)
    {
        this.entityLinkService = entityLinkService;
        this.spaceManager = spaceManager;
        this.applicationProperties = applicationProperties;
        this.userAccessor = userAccessor;
        this.labelsService = labelsService;
        this.anyTypeDao = anyTypeDao;
        this.soyTemplateRenderer = soyTemplateRenderer;
    }

    @Override
    public void init(final ActionConfiguration config)
    {
        final String noUpdatePeriodString = singleValue(config, NO_UPDATE_PERIOD_KEY);
        if (StringUtils.isNumeric(noUpdatePeriodString))
        {
            noUpdatePeriod = Long.parseLong(noUpdatePeriodString);
        }
        else
        {
            //default to 1 week
            noUpdatePeriod = 7 * 24 * 3600 * 1000;
        }

        final String issueTypeString = singleValue(config, ISSUE_TYPE_ID_KEY);
        if (StringUtils.isNotBlank(issueTypeString))
        {
            issueTypeId = issueTypeString;
        }
        else
        {
            //default to Task.
            issueTypeId = "3";
        }
    }

    @Override
    public void execute(final String actorName, final Iterable<SearchResult> items, final ErrorCollection errorCollection)
    {
        final Date now = new Date();
        final User actor = userAccessor.getUser(actorName);
        for (final SearchResult item : items)
        {
            final Set<String> labels = item.getLabels(actor);
            final long timeSinceLastUpdate = now.getTime() - item.getLastModificationDate().getTime();
            final Searchable content = (Searchable) anyTypeDao.findByHandle(item.getHandle());
            if (content == null)
            {
                //Probably means that the indexes haven't updated yet. Ignore since it should be gone on the next run of this action.
                continue;
            }

            //if the content hasn't been labelled yet and not updated for more than the update period lets create a JIRA issue!
            if (!labels.contains(UPDATE_REQUESTED_LABEL) && timeSinceLastUpdate > noUpdatePeriod)
            {
                final Space space = spaceManager.getSpace(item.getSpaceKey());
                final EntityLink jiraLink = entityLinkService.getPrimaryEntityLink(space, JiraProjectEntityType.class);
                if (jiraLink != null)
                {
                    final Gson gson = new Gson();
                    final ApplicationLinkRequestFactory requestFactory = jiraLink.getApplicationLink().createAuthenticatedRequestFactory();
                    try
                    {
                        final ApplicationLinkRequest request = requestFactory.createRequest(Request.MethodType.POST, "/rest/api/2/issue/");
                        request.setHeader("Content-Type", "application/json");

                        final JIRAIssue issue = new JIRAIssue.Builder().
                                project(jiraLink.getKey()).
                                issueTypeId(issueTypeId).
                                summary(format("Confluence %s \"%s\" requires update", item.getType(), item.getDisplayTitle())).
                                description(format("Please update [%s|%s]...", item.getDisplayTitle(), applicationProperties.getBaseUrl() + item.getUrlPath())).
                                labels(item.getHandle().toString()).
                                build();

                        request.setEntity(gson.toJson(issue));
                        request.execute(new ResponseHandler<Response>()
                        {
                            @Override
                            public void handle(final Response response) throws ResponseException
                            {
                                if (response.getStatusCode() > 300)
                                {
                                    errorCollection.addErrorMessage(format("Invalid status (%s) received from JIRA: '%s'", response.getStatusCode(), response.getResponseBodyAsString()));
                                }
                                else
                                {
                                    issuesCreated++;
                                    labelsService.newAddLabelCommand(UPDATE_REQUESTED_LABEL, actor, content.getId()).execute();
                                }
                            }
                        });
                    }
                    catch (CredentialsRequiredException e)
                    {
                        errorCollection.addErrorMessage("Unable to create request for remote JIRA instance due to missing credentials: " + e.getMessage());
                    }
                    catch (ResponseException e)
                    {
                        errorCollection.addErrorMessage("Response exception received for applinks request to JIRA: " + e.getMessage());
                    }
                }
                else
                {
                    errorCollection.addErrorMessage(format("Found content (%s - %s) that requires update but no JIRA entity link could be found. Configure Applinks first.",
                            item.getType(), item.getDisplayTitle()));
                }
            }
            //otherwise if we do have a label but we've been updated recently lets get rid of this label so the next time we run and are over the update period
            //issues get created again!
            else if (labels.contains(UPDATE_REQUESTED_LABEL) && timeSinceLastUpdate <= noUpdatePeriod)
            {
                labelsService.newRemoveLabelCommand(UPDATE_REQUESTED_LABEL, actor, content.getId()).execute();
            }
        }
    }

    @Override
    public AuditString getAuditLog()
    {
        return new DefaultAuditString(String.format("Create JIRA Issues - %d issues created", issuesCreated));
    }

    @Override
    public String getConfigurationTemplate(ActionConfiguration actionConfiguration, final String actor)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, actionConfiguration);
            return soyTemplateRenderer.render(RESOURCE_KEY, "Atlassian.Templates.Automation.Confluence.createJiraIssue", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public String getViewTemplate(final ActionConfiguration actionConfiguration, final String actor)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, actionConfiguration);
            return soyTemplateRenderer.render(RESOURCE_KEY, "Atlassian.Templates.Automation.Confluence.createJiraIssueView", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public ErrorCollection validateAddConfiguration(I18nResolver i18n, Map<String, List<String>> params, String actor)
    {
        final ErrorCollection errorCollection = new ErrorCollection();

        final String issueTypeId = singleValue(params, ISSUE_TYPE_ID_KEY);
        if (!StringUtils.isBlank(issueTypeId) && (!StringUtils.isNumeric(issueTypeId) || Integer.parseInt(issueTypeId) < 0))
        {
            errorCollection.addError(ISSUE_TYPE_ID_KEY, i18n.getText("automation.confluence.value.invalid"));
        }
        final String noUpdatePeriod = singleValue(params, NO_UPDATE_PERIOD_KEY);
        if (!StringUtils.isBlank(noUpdatePeriod) && (!StringUtils.isNumeric(noUpdatePeriod) || Integer.parseInt(noUpdatePeriod) < 0))
        {
            errorCollection.addError(NO_UPDATE_PERIOD_KEY, i18n.getText("automation.confluence.value.invalid"));
        }
        return errorCollection;
    }

    public static class JIRAIssue
    {
        private final Map<String, Object> fields = Maps.newHashMap();

        private JIRAIssue()
        {
        }

        private void addField(String key, Object field)
        {
            fields.put(key, field);
        }

        public static class Builder
        {
            private String projectKey;
            private String summary;
            private String issueTypeId;
            private String description;
            private List<String> labels = Lists.newArrayList();

            public Builder project(String key)
            {
                this.projectKey = key;
                return this;
            }

            public Builder issueTypeId(String issueTypeId)
            {
                this.issueTypeId = issueTypeId;
                return this;
            }

            public Builder summary(String summary)
            {
                this.summary = summary;
                return this;
            }

            public Builder description(String description)
            {
                this.description = description;
                return this;
            }

            public Builder labels(String... labels)
            {
                this.labels = Lists.newArrayList(labels);
                return this;
            }

            public JIRAIssue build()
            {
                final JIRAIssue issue = new JIRAIssue();
                final Map<String, String> projectParams = Maps.newHashMap();
                projectParams.put("key", this.projectKey);
                issue.addField("project", projectParams);
                issue.addField("summary", this.summary);

                final Map<String, String> issueTypeParams = Maps.newHashMap();
                issueTypeParams.put("id", issueTypeId);
                issue.addField("issuetype", issueTypeParams);
                issue.addField("description", this.description);
                if (!labels.isEmpty())
                {
                    issue.addField("labels", labels);
                }
                return issue;
            }
        }
    }
}

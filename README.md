# Atlassian Automation Plugin

## Overview
This plugin provides x-product automation for Atlassian products. You can read more on [how to use and configure this plugin ](https://blogs.atlassian.com/2014/02/atlassian-support-uses-jira-automation-plugin/).
We've put great effort into making this plugin more extendable by everyone, and there is [tutorial on how to add own actions](http://blogs.atlassian.com/2014/02/extending-jira-automation-plugin/).


## Getting started

From the top level directory run:
atlas-mvn clean install -DskipTests

cd automation-func-tests

JIRA: atlas-debug --product jira
Confluence: atlas-debug --product confluence

To test Confluence -> Create JIRA issue automation by running TestJiraIssueAction:
atlas-run --product confluence
atlas-run --product jira-clean
Then execute the test.

atlas-mvn clean install will execute all tests in all products.  To execute JIRA or Confluence tests only add:
-DtestGroups=jira-integration or -DtestGroups=confluence-integration-integration

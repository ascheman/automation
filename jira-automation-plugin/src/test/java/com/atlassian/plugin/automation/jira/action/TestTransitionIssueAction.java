package com.atlassian.plugin.automation.jira.action;

import com.atlassian.crowd.model.user.User;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueInputParameters;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.plugin.automation.config.DefaultAutomationConfiguration;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.internal.verification.Times;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.*;

@RunWith (MockitoJUnitRunner.class)
public class TestTransitionIssueAction extends TestCase
{
    @Mock
    private UserManager userManager;
    @Mock
    private IssueService issueService;
    @Mock
    private SoyTemplateRenderer soyTemplateRenderer;
    @Mock
    private I18nResolver i18nResolver;
    @Mock
    private WorkflowManager workflowManager;

    @Before
    public void setUp() throws Exception
    {
        when(i18nResolver.getText(Matchers.anyString())).thenReturn("translated text");
    }

    @Test
    public void testExecutePasses() throws Exception
    {
        TransitionIssueAction transitionIssueAction = new TransitionIssueAction(userManager, issueService, soyTemplateRenderer, workflowManager, i18nResolver);
        Map<String, List<String>> params = Maps.newHashMap();
        params.put(TransitionIssueAction.ACTION_ID_KEY, Lists.newArrayList("jira|||10"));
        params.put(TransitionIssueAction.TRANSITION_FIELDS_KEY, Lists.newArrayList("resolution=3\npriority=2"));
        transitionIssueAction.init(new DefaultAutomationConfiguration(-1, "", params));

        Issue issue = mock(MutableIssue.class);
        when(issue.getId()).thenReturn(1000l);
        List<Issue> issues = Lists.newArrayList(issue);
        ErrorCollection errorCollection = new ErrorCollection();

        final IssueService.TransitionValidationResult validationResult = mock(IssueService.TransitionValidationResult.class);
        when(issueService.validateTransition(Matchers.any(User.class), Matchers.eq(1000l), Matchers.eq(10), Matchers.any(IssueInputParameters.class))).then(new Answer<Object>()
        {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable
            {
                IssueInputParameters issueInputParameters = (IssueInputParameters) invocationOnMock.getArguments()[3];
                assertEquals("invalid resolution ID", "3", issueInputParameters.getResolutionId());
                assertEquals("invalid priority ID", "2", issueInputParameters.getPriorityId());
                return validationResult;
            }
        });
        when(validationResult.isValid()).thenReturn(true);

        IssueService.IssueResult transitionResult = mock(IssueService.IssueResult.class);
        when(transitionResult.isValid()).thenReturn(true);

        when(issueService.transition(Matchers.any(User.class), Matchers.any(IssueService.TransitionValidationResult.class))).
                thenReturn(transitionResult);

        transitionIssueAction.execute("actor", issues, errorCollection);

        verify(issueService).transition(Matchers.any(User.class), Matchers.same(validationResult));
        assertFalse("errors present", errorCollection.hasAnyErrors());
    }

    @Test
    public void testExecuteFails() throws Exception
    {
        TransitionIssueAction transitionIssueAction = new TransitionIssueAction(userManager, issueService, soyTemplateRenderer, workflowManager, i18nResolver);
        Map<String, List<String>> params = Maps.newHashMap();
        params.put(TransitionIssueAction.ACTION_ID_KEY, Lists.newArrayList("jira|||10"));
        transitionIssueAction.init(new DefaultAutomationConfiguration(-1, "", params));

        Issue issue = mock(MutableIssue.class);
        when(issue.getId()).thenReturn(1000l);
        List<Issue> issues = Lists.newArrayList(issue);
        ErrorCollection errorCollection = new ErrorCollection();

        IssueService.TransitionValidationResult validationResult = mock(IssueService.TransitionValidationResult.class);
        when(issueService.validateTransition(Matchers.any(User.class), Matchers.eq(1000l), Matchers.eq(10), Matchers.any(IssueInputParameters.class))).
                thenReturn(validationResult);

        when(validationResult.isValid()).thenReturn(false);
        final SimpleErrorCollection validationErrorCollection = new SimpleErrorCollection();
        validationErrorCollection.addErrorMessage("error");

        when(validationResult.getErrorCollection()).thenReturn(validationErrorCollection);

        transitionIssueAction.execute("actor", issues, errorCollection);

        verify(issueService, new Times(0)).transition(Matchers.any(User.class), Matchers.same(validationResult));
        assertTrue("no errors present", errorCollection.hasAnyErrors());
    }

    @Test
    public void testExecuteFailsWthValidator() throws Exception
    {
        TransitionIssueAction transitionIssueAction = new TransitionIssueAction(userManager, issueService, soyTemplateRenderer, workflowManager, i18nResolver);
        Map<String, List<String>> params = Maps.newHashMap();
        params.put(TransitionIssueAction.ACTION_ID_KEY, Lists.newArrayList("jira|||10"));
        transitionIssueAction.init(new DefaultAutomationConfiguration(-1, "", params));

        Issue issue = mock(MutableIssue.class);
        when(issue.getId()).thenReturn(1000l);
        List<Issue> issues = Lists.newArrayList(issue);
        ErrorCollection errorCollection = new ErrorCollection();

        IssueService.TransitionValidationResult validationResult = mock(IssueService.TransitionValidationResult.class);
        when(issueService.validateTransition(Matchers.any(User.class), Matchers.eq(1000l), Matchers.eq(10), Matchers.any(IssueInputParameters.class))).
                thenReturn(validationResult);

        when(validationResult.isValid()).thenReturn(true);

        IssueService.IssueResult transitionResult = mock(IssueService.IssueResult.class);
        when(issueService.transition(Matchers.any(User.class), Matchers.any(IssueService.TransitionValidationResult.class))).
                thenReturn(transitionResult);

        final SimpleErrorCollection validationErrorCollection = new SimpleErrorCollection();
        validationErrorCollection.addErrorMessage("error");

        when(transitionResult.isValid()).thenReturn(false);
        when(transitionResult.getErrorCollection()).thenReturn(validationErrorCollection);

        transitionIssueAction.execute("actor", issues, errorCollection);

        verify(issueService).transition(Matchers.any(User.class), Matchers.same(validationResult));
        assertTrue("no errors present", errorCollection.hasAnyErrors());
    }

    @Test
    public void testValidateAddConfiguration() throws Exception
    {
        TransitionIssueAction transitionIssueAction = new TransitionIssueAction(userManager, issueService, soyTemplateRenderer, workflowManager, i18nResolver);
        Map<String, List<String>> params = Maps.newHashMap();
        assertTrue("no error produced", transitionIssueAction.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        params.put(TransitionIssueAction.ACTION_ID_KEY, Lists.newArrayList("jira|||a"));
        assertTrue("no error produced", transitionIssueAction.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        params.put(TransitionIssueAction.ACTION_ID_KEY, Lists.newArrayList("jira|||10"));
        assertFalse("error produced", transitionIssueAction.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());
    }

    @Test
    public void testValidateAddConfigurationFieldsConfig() throws Exception
    {
        TransitionIssueAction transitionIssueAction = new TransitionIssueAction(userManager, issueService, soyTemplateRenderer, workflowManager, i18nResolver);
        Map<String, List<String>> params = Maps.newHashMap();
        params.put(TransitionIssueAction.ACTION_ID_KEY, Lists.newArrayList("jira|||10"));
        params.put(TransitionIssueAction.TRANSITION_FIELDS_KEY, Lists.newArrayList(""));
        assertFalse("error produced", transitionIssueAction.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        params.put(TransitionIssueAction.TRANSITION_FIELDS_KEY, Lists.newArrayList("a"));
        assertTrue("no error produced", transitionIssueAction.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        params.put(TransitionIssueAction.TRANSITION_FIELDS_KEY, Lists.newArrayList("a="));
        assertTrue("no error produced", transitionIssueAction.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        params.put(TransitionIssueAction.TRANSITION_FIELDS_KEY, Lists.newArrayList("a=10"));
        assertFalse("error produced", transitionIssueAction.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());
    }
}

package com.atlassian.plugin.automation.jira.trigger;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.type.EventTypeManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.customfields.impl.UserCFType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.jql.parser.JqlQueryParser;
import com.atlassian.jira.jql.query.LuceneQueryBuilder;
import com.atlassian.jira.plugin.userformat.UserFormats;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Map;

import static com.google.common.collect.Lists.newArrayList;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class TestIssueEventTrigger
{
    @Mock
    private SoyTemplateRenderer soyTemplateRenderer;
    @Mock
    private I18nResolver i18nResolver;
    @Mock
    private EventTypeManager eventTypeManager;
    @Mock
    private JqlQueryParser jqlQueryParser;
    @Mock
    private SearchService searchService;
    @Mock
    private UserManager userManager;
    @Mock
    private LuceneQueryBuilder luceneQueryBuilder;
    @Mock
    private CustomFieldManager customFieldManager;
    @Mock
    private UserFormats userFormats;

    @Before
    public void setUp() throws Exception
    {
        when(i18nResolver.getText(Matchers.anyString())).thenReturn("translated text");
        ComponentAccessor.Worker worker = Mockito.mock(ComponentAccessor.Worker.class);
        ComponentAccessor.initialiseWorker(worker);

        when(ComponentAccessor.getComponentOfType(LuceneQueryBuilder.class)).thenReturn(luceneQueryBuilder);
    }

    @Test
    public void testValidateAddConfiguration() throws Exception
    {
        final IssueEventTrigger issueEventTrigger = new IssueEventTrigger(soyTemplateRenderer, eventTypeManager,
                jqlQueryParser, searchService, userManager, customFieldManager, i18nResolver, userFormats);
        final Map<String, List<String>> params = Maps.newHashMap();
        assertTrue("no errors present", issueEventTrigger.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        params.put(IssueEventTrigger.EVENT_ID_KEY, newArrayList(""));
        assertTrue("no errors present", issueEventTrigger.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        params.put(IssueEventTrigger.EVENT_ID_KEY, newArrayList("a"));
        assertTrue("no errors present", issueEventTrigger.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        params.put(IssueEventTrigger.EVENT_ID_KEY, newArrayList("10"));
        assertFalse("errors present", issueEventTrigger.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        params.put(IssueEventTrigger.RESTRICT_EVENT_AUTHORS, newArrayList("true"));
        params.put(IssueEventTrigger.USER_CF_ID, newArrayList("doesntexist"));
        assertTrue("no errors present", issueEventTrigger.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        params.put(IssueEventTrigger.USER_CF_ID, newArrayList("customfield_10001"));
        final CustomField mockCF = Mockito.mock(CustomField.class);
        final UserCFType mockCFType = Mockito.mock(UserCFType.class);

        when(mockCF.getCustomFieldType()).thenReturn(mockCFType);
        when(customFieldManager.getCustomFieldObject("customfield_10001")).thenReturn(mockCF);
        assertFalse("errors present", issueEventTrigger.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        params.put(IssueEventTrigger.SPECIFIC_USER, newArrayList("invaliuser"));
        when(userManager.getUser("invaliduser")).thenReturn(null);
        assertTrue("no errors present", issueEventTrigger.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        final User mockUser = Mockito.mock(User.class);
        params.put(IssueEventTrigger.SPECIFIC_USER, newArrayList("fred"));
        when(userManager.getUser("fred")).thenReturn(mockUser);
        assertFalse("errors present", issueEventTrigger.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());
    }
}

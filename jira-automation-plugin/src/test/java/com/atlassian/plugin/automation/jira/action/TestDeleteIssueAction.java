package com.atlassian.plugin.automation.jira.action;

import com.atlassian.crowd.model.user.User;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.plugin.automation.config.DefaultAutomationConfiguration;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.internal.verification.Times;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TestDeleteIssueAction
{
    @Mock
    private UserManager userManager;
    @Mock
    private IssueService issueService;
    @Mock
    private SoyTemplateRenderer soyTemplateRenderer;
    @Mock
    private I18nResolver i18nResolver;

    @Before
    public void setUp() throws Exception
    {
        when(i18nResolver.getText(Matchers.anyString())).thenReturn("translated text");
    }

    @Test
    public void testExecutePasses() throws Exception
    {
        final DeleteIssueAction deleteIssueAction = new DeleteIssueAction(soyTemplateRenderer, userManager, issueService);
        Map<String, List<String>> params = Maps.newHashMap();
        params.put(DeleteIssueAction.DELETE_NOTIFICATION_KEY, Lists.newArrayList("true"));
        deleteIssueAction.init(new DefaultAutomationConfiguration(-1, "", params));

        Issue issue = mock(MutableIssue.class);
        when(issue.getId()).thenReturn(1000l);
        List<Issue> issues = Lists.newArrayList(issue);
        ErrorCollection errorCollection = new ErrorCollection();

        ApplicationUser applicationUser = mock(ApplicationUser.class);
        when(userManager.getUserByName(Matchers.anyString())).thenReturn(applicationUser);

        final IssueService.DeleteValidationResult validationResult = mock(IssueService.DeleteValidationResult.class);
        when(issueService.validateDelete(Matchers.any(User.class), Matchers.eq(1000l))).thenReturn(validationResult);
        when(validationResult.isValid()).thenReturn(true);

        final com.atlassian.jira.util.ErrorCollection deleteResult = new SimpleErrorCollection();
        when(issueService.delete(Matchers.any(User.class), Matchers.any(IssueService.DeleteValidationResult.class),
                eq(EventDispatchOption.ISSUE_DELETED), eq(true))).thenReturn(deleteResult);

        deleteIssueAction.execute("actor", issues, errorCollection);

        verify(issueService).delete(Matchers.any(User.class), Matchers.same(validationResult), Matchers.same(EventDispatchOption.ISSUE_DELETED), Matchers.eq(true));
        assertFalse("errors present", errorCollection.hasAnyErrors());
    }

    @Test
    public void testExecuteFails() throws Exception
    {

        final DeleteIssueAction deleteIssueAction = new DeleteIssueAction(soyTemplateRenderer, userManager, issueService);
        Map<String, List<String>> params = Maps.newHashMap();
        deleteIssueAction.init(new DefaultAutomationConfiguration(-1, "", params));

        Issue issue = mock(MutableIssue.class);
        when(issue.getId()).thenReturn(1000l);
        List<Issue> issues = Lists.newArrayList(issue);
        ErrorCollection errorCollection = new ErrorCollection();

        ApplicationUser applicationUser = mock(ApplicationUser.class);
        when(userManager.getUserByName(Matchers.anyString())).thenReturn(applicationUser);

        final IssueService.DeleteValidationResult validationResult = mock(IssueService.DeleteValidationResult.class);
        when(issueService.validateDelete(Matchers.any(User.class), Matchers.eq(1000l))).thenReturn(validationResult);
        when(validationResult.isValid()).thenReturn(false);

        deleteIssueAction.execute("actor", issues, errorCollection);
        verify(issueService, new Times(0)).delete(Matchers.any(User.class), Matchers.same(validationResult), Matchers.same(EventDispatchOption.ISSUE_DELETED), Matchers.eq(false));
    }
}

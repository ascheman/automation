package com.atlassian.plugin.automation.jira.action;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.plugin.userformat.UserFormats;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.plugin.automation.config.DefaultAutomationConfiguration;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class TestSetAssigneeToLastCommentedAction
{

    @Mock
    private GroupManager groupManager;
    @Mock
    private UserUtil userUtil;
    @Mock
    private CommentManager commentManager;
    @Mock
    private IssueService issueService;
    @Mock
    private SoyTemplateRenderer soyTemplateRenderer;
    @Mock
    private I18nResolver i18nResolver;
    @Mock
    private UserFormats userFormats;

    private SetAssigneeToLastCommentedAction action;
    private Comment comment1;
    private Comment comment2;
    private User mockCustomer;
    private User mockSupporter;
    private Issue issue;
    private User actor;

    @Before
    public void setUp()
    {
        action = new SetAssigneeToLastCommentedAction(commentManager, groupManager, issueService, userUtil, soyTemplateRenderer, userFormats);

        comment1 = Mockito.mock(Comment.class);
        comment2 = Mockito.mock(Comment.class);

        actor = Mockito.mock(User.class);
        mockCustomer = Mockito.mock(User.class);
        mockSupporter = Mockito.mock(User.class);

        when(mockCustomer.getName()).thenReturn("customer");
        when(mockSupporter.getName()).thenReturn("supporter");

        issue = Mockito.mock(Issue.class);
        when(issue.getId()).thenReturn(10000L);

        when(commentManager.getComments(issue)).thenReturn(Lists.newArrayList(comment1, comment2));
    }

    @Test
    public void testValidationInvalidGroup()
    {
        final Map<String, List<String>> params = Maps.newHashMap();
        params.put("groupMembership", Lists.newArrayList("invalidgroup"));
        final ErrorCollection errors = action.validateAddConfiguration(i18nResolver, params, "admin");

        assertTrue(errors.hasAnyErrors());
    }

    @Test
    public void testValidation()
    {
        final Map<String, List<String>> params = Maps.newHashMap();
        params.put("groupMembership", Lists.newArrayList("jira-devs"));

        final Group group = Mockito.mock(Group.class);
        when(groupManager.getGroup("jira-devs")).thenReturn(group);

        final ErrorCollection errors = action.validateAddConfiguration(i18nResolver, params, "admin");

        assertFalse(errors.hasAnyErrors());
    }

    @Test
    public void testAssignWithNoMembersOfGroup()
    {
        final ErrorCollection errorCollection = new ErrorCollection();

        final HashMap<String, List<String>> params = Maps.newHashMap();
        params.put("groupMembership", Lists.newArrayList("supporters"));
        action.init(new DefaultAutomationConfiguration(1, "stuff", params));

        final Group supportersGroup = Mockito.mock(Group.class);
        when(groupManager.getGroup("supporters")).thenReturn(supportersGroup);

        when(comment1.getAuthorUser()).thenReturn(mockCustomer);
        when(comment2.getAuthorUser()).thenReturn(mockCustomer);
        when(groupManager.isUserInGroup(mockCustomer, supportersGroup)).thenReturn(false);

        action.execute("admin", Lists.newArrayList(issue), errorCollection);

        verifyZeroInteractions(issueService);
    }

    @Test
    public void testAssignWithMembersOfGroup()
    {
        final ErrorCollection errorCollection = new ErrorCollection();

        final HashMap<String, List<String>> params = Maps.newHashMap();
        params.put("groupMembership", Lists.newArrayList("supporters"));
        action.init(new DefaultAutomationConfiguration(1, "stuff", params));

        final Group supportersGroup = Mockito.mock(Group.class);
        when(groupManager.getGroup("supporters")).thenReturn(supportersGroup);

        when(comment1.getAuthorUser()).thenReturn(mockSupporter);
        when(comment2.getAuthorUser()).thenReturn(mockCustomer);
        when(groupManager.isUserInGroup(mockSupporter, supportersGroup)).thenReturn(true);
        when(groupManager.isUserInGroup(mockCustomer, supportersGroup)).thenReturn(false);

        when(userUtil.getUser("admin")).thenReturn(actor);
        final IssueService.AssignValidationResult assignValidationResult = new IssueService.AssignValidationResult(null, new SimpleErrorCollection(), null);
        when(issueService.validateAssign(actor, 10000L, "supporter")).thenReturn(assignValidationResult);

        action.execute("admin", Lists.newArrayList(issue), errorCollection);

        verify(issueService).assign(eq(actor), eq(assignValidationResult));
    }

    @Test
    public void testAssign()
    {
        final ErrorCollection errorCollection = new ErrorCollection();

        when(comment1.getAuthorUser()).thenReturn(mockSupporter);
        when(comment2.getAuthorUser()).thenReturn(mockCustomer);

        when(userUtil.getUser("admin")).thenReturn(actor);
        final IssueService.AssignValidationResult assignValidationResult = new IssueService.AssignValidationResult(null, new SimpleErrorCollection(), null);
        when(issueService.validateAssign(actor, 10000L, "customer")).thenReturn(assignValidationResult);

        action.execute("admin", Lists.newArrayList(issue), errorCollection);

        verify(issueService).assign(eq(actor), eq(assignValidationResult));
    }

    @Test
    public void testAssignWithExcludedUsers()
    {
        //here we don't want that the customer gets the issue assigned

        final ErrorCollection errorCollection = new ErrorCollection();

        final HashMap<String, List<String>> params = Maps.newHashMap();
        params.put("excludedUsers", Lists.newArrayList("customer"));

        when(userUtil.getUser("supporter")).thenReturn(mockSupporter);
        when(userUtil.getUser("customer")).thenReturn(mockCustomer);

        action.init(new DefaultAutomationConfiguration(1, "stuff", params));

        when(comment1.getAuthorUser()).thenReturn(mockSupporter);
        when(comment2.getAuthorUser()).thenReturn(mockCustomer);

        when(userUtil.getUser("admin")).thenReturn(actor);

        final IssueService.AssignValidationResult assignValidationResult = new IssueService.AssignValidationResult(null, new SimpleErrorCollection(), null);
        when(issueService.validateAssign(actor, 10000L, "supporter")).thenReturn(assignValidationResult);

        action.execute("admin", Lists.newArrayList(issue), errorCollection);

        verify(issueService).assign(eq(actor), eq(assignValidationResult));
    }

}

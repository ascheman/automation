(function() {
    AJS.$(AJS).bind("AJS.automation.form.loaded", function(e, context) {

        new AJS.MultiSelect({
            element:AJS.$("#jiraEventId", context),
            itemAttrDisplayed: "label"
        });

        var $restrictAuthors = AJS.$("#restrictEventAuthors" ,context);
        $restrictAuthors.change(function(e) {
            e.preventDefault();
            if($restrictAuthors.is(":checked")) {
                AJS.$(".event-authors", context).show();
            } else {
                AJS.$(".event-authors", context).hide();
            }
        });
        $restrictAuthors.change();

        AJS.automation.AjaxUserPickerUtils.singleSelect("specificUser", context);
    });
})();
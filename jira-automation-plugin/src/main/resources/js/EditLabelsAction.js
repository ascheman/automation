(function () {
    AJS.$(AJS).bind("AJS.automation.form.loaded", function (e, context) {
        new JIRA.LabelPicker({
            element: AJS.$("#jiraEditLabelsAddField", context),
            ajaxOptions: {
                url: AJS.contextPath() + '/rest/api/1.0/labels/suggest'
            }
        });

        new JIRA.LabelPicker({
            element: AJS.$("#jiraEditLabelsRemoveField", context),
            ajaxOptions: {
                url: AJS.contextPath() + '/rest/api/1.0/labels/suggest'
            }
        });
    });
})();
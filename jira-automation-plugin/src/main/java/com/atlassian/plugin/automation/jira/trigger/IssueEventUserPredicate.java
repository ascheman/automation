package com.atlassian.plugin.automation.jira.trigger;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.UserField;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.plugin.automation.core.AutomationConfiguration;
import com.google.common.base.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.annotation.Nullable;

import static com.atlassian.plugin.automation.util.ParameterUtil.singleValue;


public class IssueEventUserPredicate implements Predicate<IssueEvent>
{
    private static final Logger log = Logger.getLogger(IssueEventUserPredicate.class);
    private final CustomFieldManager customFieldManager;
    private final boolean restrictEventAuthors;

    private boolean currentIsAssignee;
    private boolean currentIsReporter;
    private String userCustomFieldId;
    private String specificUser;

    public IssueEventUserPredicate(final AutomationConfiguration config, final CustomFieldManager customFieldManager)
    {
        this.customFieldManager = customFieldManager;
        currentIsAssignee = Boolean.parseBoolean(singleValue(config, "currentIsAssignee"));
        currentIsReporter = Boolean.parseBoolean(singleValue(config, "currentIsReporter"));
        userCustomFieldId = singleValue(config, "userCustomFieldId");
        specificUser = StringUtils.trim(singleValue(config, "specificUser"));
        restrictEventAuthors = Boolean.parseBoolean(singleValue(config, "restrictEventAuthors"));
    }

    @Override
    public boolean apply(@Nullable final IssueEvent event)
    {
        if (event == null)
        {
            return false;
        }
        final Issue issue = event.getIssue();
        final ApplicationUser eventAuthor = getApplicationUser(getEventAuthor(event));

        if (!restrictEventAuthors)
        {
            return true;
        }

        // if no conditions are set - proceed
        if (!currentIsAssignee && !currentIsReporter && StringUtils.isBlank(userCustomFieldId) && StringUtils.isBlank(specificUser))
        {
            return true;
        }

        // If any conditions are set, any one of them evaluating to true will cause us to transition.
        if (currentIsReporter && issue.getReporter() != null && getApplicationUser(issue.getReporter()).getKey().equals(eventAuthor.getKey()))
        {
            return true;
        }
        else if (log.isDebugEnabled())
        {
            log.debug("User " + eventAuthor.getName() + " is not the current reporter for issue " + issue.getKey());
        }

        if (currentIsAssignee && issue.getAssignee() != null && getApplicationUser(issue.getAssignee()).getKey().equals(eventAuthor.getKey()))
        {
            return true;
        }
        else if (log.isDebugEnabled())
        {
            log.debug("User " + eventAuthor.getName() + " is not the current assignee for issue " + issue.getKey());
        }

        if (userCustomFieldId != null && isInCustomField(issue, eventAuthor))
        {
            return true;
        }
        else if (log.isDebugEnabled())
        {
            log.debug("User " + eventAuthor.getName() + " is not found in required custom field " + userCustomFieldId + " for issue " + issue.getKey());
        }

        if (specificUser != null && specificUser.equals(eventAuthor.getKey()))
        {
            return true;
        }
        else if (log.isDebugEnabled())
        {
            log.debug("User " + eventAuthor.getName() + " is not user " + specificUser);
        }

        return false;
    }

    public String getSpecificUser()
    {
        return specificUser;
    }

    public CustomField getRestrictingCustomField()
    {
        return getRestrictingCustomField(userCustomFieldId);
    }

    private User getEventAuthor(IssueEvent event)
    {
        final User user = event.getUser();
        // not quite sure why this is needed. It was copied over from the toolkit auto transition listener.
        // @skarimov didn't leave a comment about why it's needed when he introduced it
        if (user == null)
        {
            return event.getComment().getAuthorUser();
        }
        return user;
    }

    /**
     * @return Whether we restrict to users in a userpicker custom field.
     */
    private boolean isInCustomField(Issue issue, ApplicationUser eventAuthor)
    {
        final CustomField cf = getRestrictingCustomField(userCustomFieldId);
        if (cf == null)
        {
            log.debug("Required custom field " + userCustomFieldId + " is not found for issue " + issue.getKey());
            return false;
        }

        @SuppressWarnings ("unchecked")
        final Iterable<ApplicationUser> cfUsers = (Iterable<ApplicationUser>) issue.getCustomFieldValue(cf);
        if (cfUsers == null)
        {
            log.debug("Required custom field " + userCustomFieldId + " has no values for issue " + issue.getKey());
            return false;
        }

        for (ApplicationUser cfUser : cfUsers)
        {
            if (cfUser.getKey().equals(eventAuthor.getKey()))
            {
                return true;
            }
        }
        return false;
    }

    //here for testing.
    ApplicationUser getApplicationUser(final User user)
    {
        return ApplicationUsers.from(user);
    }

    /**
     * Parse the user's entry (if any) for a user picker custom field.
     *
     * @param customfieldId eg. "customfield_10000"
     * @return The specified custom field.
     */
    private CustomField getRestrictingCustomField(String customfieldId)
    {
        if (StringUtils.isBlank(customfieldId))
        {
            return null;
        }

        final CustomField field = customFieldManager.getCustomFieldObject(customfieldId);
        if (field != null && field.getCustomFieldType() instanceof UserField)
        {
            return field;
        }
        else
        {
            log.error("Issue Event Trigger configured to restrict to custom field with id '" +
                    customfieldId + "', but no such custom field was found. Value expected to be customfield_<id>, eg. customfield_10000.");
            return null;
        }
    }
}

package com.atlassian.plugin.automation.jira.trigger;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.event.type.EventTypeManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.UserField;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.jql.parser.JqlParseException;
import com.atlassian.jira.jql.parser.JqlQueryParser;
import com.atlassian.jira.plugin.userformat.UserFormats;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.plugin.automation.core.EventTrigger;
import com.atlassian.plugin.automation.core.TriggerContext;
import com.atlassian.plugin.automation.core.auditlog.AuditString;
import com.atlassian.plugin.automation.core.auditlog.DefaultAuditString;
import com.atlassian.plugin.automation.core.trigger.TriggerConfiguration;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.plugin.automation.util.ParameterUtil;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.query.Query;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nullable;
import javax.inject.Inject;

import static com.atlassian.plugin.automation.jira.util.Constants.CONFIG_COMPLETE_KEY;
import static com.atlassian.plugin.automation.util.ParameterUtil.singleValue;

public class IssueEventTrigger implements EventTrigger<Issue>
{
    private static final Logger log = Logger.getLogger(IssueEventTrigger.class);

    private static final String PROFILE_LINK_USER_FORMAT_TYPE = "profileLinkWithAvatar";

    public static final String EVENT_ID_KEY = "jiraEventId";
    public static final String EVENT_JQL_KEY = "jiraJqlExpression";
    public static final String USER_CF_ID = "userCustomFieldId";
    public static final String SPECIFIC_USER = "specificUser";
    public static final String RESTRICT_EVENT_AUTHORS = "restrictEventAuthors";

    private static final String RESOURCE_KEY = "com.atlassian.plugin.automation.jira-automation-plugin:jira-config-resources";
    private static final String JIRA_EVENTS = "jiraEvents";

    private final SoyTemplateRenderer soyTemplateRenderer;
    private final EventTypeManager eventTypeManager;
    private final JqlQueryParser jqlQueryParser;
    private final CustomFieldManager customFieldManager;
    private final SearchService searchService;
    private final UserManager userManager;
    private final I18nResolver i18n;
    private final UserFormats userFormats;
    private final Set<Long> eventIds = Sets.newHashSet();

    private String jql;
    private Predicate<IssueEvent> issueEventUserPredicate;

    @Inject
    public IssueEventTrigger(
            @ComponentImport final SoyTemplateRenderer soyTemplateRenderer,
            @ComponentImport final EventTypeManager eventTypeManager,
            @ComponentImport final JqlQueryParser jqlQueryParser,
            @ComponentImport final SearchService searchService,
            @ComponentImport final UserManager userManager,
            @ComponentImport final CustomFieldManager customFieldManager,
            @ComponentImport final I18nResolver i18nResolver,
            @ComponentImport final UserFormats userFormats)
    {
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.eventTypeManager = eventTypeManager;
        this.jqlQueryParser = jqlQueryParser;
        this.customFieldManager = customFieldManager;
        this.searchService = searchService;
        this.userManager = userManager;
        this.i18n = i18nResolver;
        this.userFormats = userFormats;
    }

    @Override
    public void init(TriggerConfiguration config)
    {
        eventIds.addAll(transformEventIds(config));
        jql = singleValue(config, EVENT_JQL_KEY);
        issueEventUserPredicate = new IssueEventUserPredicate(config, customFieldManager);
    }

    @Override
    public Iterable<Issue> getItems(TriggerContext context, ErrorCollection errorCollection)
    {
        IssueEvent issueEvent = (IssueEvent) context.getSourceEvent();
        if (issueEvent != null)
        {
            if (eventIds.contains(issueEvent.getEventTypeId()) && issueEventUserPredicate.apply(issueEvent))
            {
                final Issue issue = issueEvent.getIssue();

                if (StringUtils.isBlank(jql))
                {
                    return Lists.newArrayList(issue);
                }

                try
                {
                    final User user = userManager.getUserByName(context.getActor()).getDirectoryUser();
                    final Query query = jqlQueryParser.parseQuery(jql);
                    final Query issueQuery = JqlQueryBuilder.newBuilder().where().issue().eq(issue.getKey()).and().addClause(query.getWhereClause()).buildQuery();
                    if (log.isDebugEnabled())
                    {
                        log.debug("Trying to match issue '" + issue.getKey() + "' using query '" + issueQuery + "'");
                    }

                    final long totalHits = searchService.searchCount(user, issueQuery);

                    if (log.isDebugEnabled())
                    {
                        log.debug("Matched " + totalHits + " issues using query '" + issueQuery + "'");
                    }
                    if (totalHits > 0)
                    {
                        return Lists.newArrayList(issue);
                    }
                    else
                    {
                        return Collections.emptyList();
                    }
                }
                catch (JqlParseException e)
                {
                    log.error("Error parsing query '" + jql + "'. No issue will be processed.", e);
                    errorCollection.addErrorMessage("Error parsing query '" + jql + "'. No issue will be processed.");
                }
                catch (SearchException e)
                {
                    log.error("Error searching for '" + jql + "'. No issue will be processed.", e);
                    errorCollection.addErrorMessage("Error searching for '" + jql + "'. No issue will be processed.");
                }
            }
        }

        return Lists.newArrayList();
    }

    @Override
    public AuditString getAuditLog()
    {
        return new DefaultAuditString(String.format("Issue Event trigger for events %s", Arrays.toString(eventIds.toArray())));
    }

    @Override
    public String getConfigurationTemplate(TriggerConfiguration triggerConfiguration, final String actor)
    {
        try
        {
            final Set<Long> eventIds = transformEventIds(triggerConfiguration);
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, triggerConfiguration);

            final Iterable events = Iterables.transform(eventTypeManager.getEventTypes(), new Function<EventType, RenderableEvent>()
            {
                @Override
                public RenderableEvent apply(@Nullable final EventType input)
                {
                    if (input != null)
                    {
                        return new RenderableEvent(input.getId(), input.getNameKey(), eventIds.contains(input.getId()));
                    }
                    return null;
                }
            });

            context.put(JIRA_EVENTS, events);

            final Iterable<CustomField> userCFs = Iterables.filter(customFieldManager.getCustomFieldObjects(), new Predicate<CustomField>()
            {
                @Override
                public boolean apply(@Nullable final CustomField input)
                {
                    return input != null && input.getCustomFieldType() instanceof UserField;
                }
            });
            if (!Iterables.isEmpty(userCFs))
            {
                context.put("userCFs", userCFs);
            }

            if (triggerConfiguration != null)
            {
                final String specificUser = singleValue(triggerConfiguration.getParameters(), SPECIFIC_USER);
                if (StringUtils.isNotBlank(specificUser))
                {
                    final User user = userManager.getUser(specificUser.trim());
                    if (user != null)
                    {
                        context.put(SPECIFIC_USER, user);
                    }
                }
            }

            return soyTemplateRenderer.render(RESOURCE_KEY, "Atlassian.Templates.Automation.JIRA.issueEvent", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public String getViewTemplate(final TriggerConfiguration config, final String actor)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, config);

            final Set<Long> eventIds = transformEventIds(config);
            final List<String> eventNames = Lists.newArrayList();
            for (Long eventId : eventIds)
            {
                eventNames.add(i18n.getText(eventTypeManager.getEventType(eventId).getNameKey()));
            }
            Collections.sort(eventNames);
            context.put(JIRA_EVENTS, eventNames);

            final IssueEventUserPredicate issueEventUserPredicate = new IssueEventUserPredicate(config, customFieldManager);
            final CustomField restrictingCustomField = issueEventUserPredicate.getRestrictingCustomField();
            if (restrictingCustomField != null)
            {
                context.put("userCF", restrictingCustomField.getFieldName());
            }
            if (issueEventUserPredicate.getSpecificUser() != null)
            {
                context.put("specificUser", userFormats.formatter(PROFILE_LINK_USER_FORMAT_TYPE).formatUserkey(issueEventUserPredicate.getSpecificUser(), "retricted-user-" + config.getId()));
            }

            return soyTemplateRenderer.render(CONFIG_COMPLETE_KEY, "Atlassian.Templates.Automation.JIRA.issueEventView", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public ErrorCollection validateAddConfiguration(I18nResolver i18n, Map<String, List<String>> params, final String actor)
    {
        final ErrorCollection errorCollection = new ErrorCollection();

        if (!params.containsKey(EVENT_ID_KEY))
        {
            errorCollection.addError(EVENT_ID_KEY, i18n.getText("automation.jira.eventid.invalid"));
        }
        else
        {
            for (String eventId : params.get(EVENT_ID_KEY))
            {
                if (StringUtils.isBlank(eventId) || !StringUtils.isNumeric(eventId))
                {
                    errorCollection.addError(EVENT_ID_KEY, i18n.getText("automation.jira.eventid.invalid"));
                    break;
                }
            }
        }

        final String jqlString = singleValue(params, EVENT_JQL_KEY);
        if (StringUtils.isNotBlank(jqlString))
        {
            final User user = userManager.getUser(actor);
            final SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlString);

            if (!parseResult.isValid())
            {
                errorCollection.addError(EVENT_JQL_KEY, i18n.getText("automation.jira.jql.invalid"));
            }
            else
            {
                // Validation is now here, because here we still have HTTP session to validate the JQL (see JRA-33499)
                final MessageSet messageSet = searchService.validateQuery(user, parseResult.getQuery());
                if (messageSet.hasAnyErrors())
                {
                    errorCollection.addError(EVENT_JQL_KEY, messageSet.getErrorMessages().toString());
                }
                else if (messageSet.hasAnyWarnings())
                {
                    errorCollection.addError(EVENT_JQL_KEY, messageSet.getWarningMessages().toString());
                }
            }
        }

        //validate restrict event author options.
        if (Boolean.parseBoolean(singleValue(params, RESTRICT_EVENT_AUTHORS)))
        {
            final String customFieldId = singleValue(params, USER_CF_ID);
            if (StringUtils.isNotBlank(customFieldId))
            {
                final CustomField customFieldObject = customFieldManager.getCustomFieldObject(customFieldId);
                if (customFieldObject == null || !(customFieldObject.getCustomFieldType() instanceof UserField))
                {
                    errorCollection.addError(USER_CF_ID, i18n.getText("automation.jira.event.restrict.cf.error"));
                }
            }
            final String specificUser = singleValue(params, SPECIFIC_USER);
            if (StringUtils.isNotBlank(specificUser))
            {
                final User user = userManager.getUser(specificUser.trim());
                if (user == null)
                {
                    errorCollection.addError(SPECIFIC_USER, i18n.getText("automation.jira.event.restrict.user.error", specificUser.trim()));
                }
            }
        }


        return errorCollection;
    }

    @Override
    public String getEventClassName()
    {
        return "com.atlassian.jira.event.issue.IssueEvent";
    }

    private Set<Long> transformEventIds(final TriggerConfiguration config)
    {
        final Set<Long> ret = Sets.newHashSet();
        if (config != null && config.getParameters().containsKey(EVENT_ID_KEY))
        {
            for (String eventIdString : config.getParameters().get(EVENT_ID_KEY))
            {
                if (StringUtils.isNotBlank(eventIdString) && StringUtils.isNumeric(eventIdString))
                {
                    ret.add(Long.parseLong(eventIdString));
                }
            }
        }
        return ret;
    }

    public static final class RenderableEvent
    {
        private Long id;
        private String nameKey;
        private boolean selected;

        public RenderableEvent(final Long id, final String nameKey, final boolean selected)
        {
            this.id = id;
            this.nameKey = nameKey;
            this.selected = selected;
        }

        public Long getId()
        {
            return id;
        }

        public String getNameKey()
        {
            return nameKey;
        }

        public boolean isSelected()
        {
            return selected;
        }
    }
}

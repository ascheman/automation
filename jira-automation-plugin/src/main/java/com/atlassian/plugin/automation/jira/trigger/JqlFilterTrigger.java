package com.atlassian.plugin.automation.jira.trigger;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.plugin.automation.core.AbstractCronTrigger;
import com.atlassian.plugin.automation.core.TriggerContext;
import com.atlassian.plugin.automation.core.auditlog.AuditString;
import com.atlassian.plugin.automation.core.auditlog.DefaultAuditString;
import com.atlassian.plugin.automation.core.trigger.TriggerConfiguration;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.plugin.automation.util.ParameterUtil;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.query.Query;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import static com.atlassian.plugin.automation.jira.util.Constants.CONFIG_COMPLETE_KEY;
import static com.atlassian.plugin.automation.util.ParameterUtil.singleValue;

/**
 * Implements trigger for JQL filter
 */
public class JqlFilterTrigger extends AbstractCronTrigger<Issue>
{
    private static final Logger log = Logger.getLogger(JqlFilterTrigger.class);
    public static final String MAX_RESULTS_KEY = "jiraMaxResults";
    public static final String JQL_KEY = "jiraJqlExpression";

    private final SearchService searchService;
    private final UserManager userManager;
    private final ApplicationProperties applicationProperties;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final DateTimeFormatter dateTimeFormatter;
    private String jql;
    private int maxResults;

    @Inject
    public JqlFilterTrigger(
            @ComponentImport final SearchService searchService,
            @ComponentImport final UserManager userManager,
            @ComponentImport final ApplicationProperties applicationProperties,
            @ComponentImport final SoyTemplateRenderer soyTemplateRenderer,
            @ComponentImport final DateTimeFormatter dateTimeFormatter)
    {
        this.searchService = searchService;
        this.userManager = userManager;
        this.applicationProperties = applicationProperties;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.dateTimeFormatter = dateTimeFormatter;
    }

    @Override
    public void init(TriggerConfiguration config)
    {
        super.init(config);
        jql = singleValue(config, JQL_KEY);
        String maxResultsParam = singleValue(config, MAX_RESULTS_KEY);

        if (StringUtils.isBlank(maxResultsParam))
        {
            maxResultsParam = applicationProperties.getDefaultBackedString(APKeys.JIRA_SEARCH_VIEWS_MAX_LIMIT);
        }
        maxResults = Integer.parseInt(maxResultsParam);
    }

    @Override
    public Iterable<Issue> getItems(TriggerContext context, ErrorCollection errorCollection)
    {
        final User user = userManager.getUser(context.getActor());
        log.debug(String.format("Running JQLFilterTrigger with actor '%s' and jql: '%s'", context.getActor(), jql));

        if (!StringUtils.isBlank(jql))
        {
            final Query query = tryParseQuery(user, jql, errorCollection);
            if (query != null)
            {
                try
                {
                    final SearchResults searchResults = searchService.search(user, query, new PagerFilter(maxResults));
                    log.debug("# Matched issues: " + searchResults.getTotal());
                    return searchResults.getIssues();
                }
                catch (SearchException e)
                {
                    log.error("Unable to run search for JQL: " + jql, e);
                    errorCollection.addErrorMessage("Unable to run search for JQL: " + jql);
                }
            }
        }
        return Lists.newArrayList();
    }

    @Override
    public AuditString getAuditLog()
    {
        return new DefaultAuditString(String.format("JIRA JQL trigger (%s)", jql));
    }

    @Override
    public String getConfigurationTemplate(TriggerConfiguration triggerConfiguration, final String actor)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, triggerConfiguration);
            context.put("currentServerTime", dateTimeFormatter.withSystemZone().format(new Date()));
            return soyTemplateRenderer.render(CONFIG_COMPLETE_KEY, "Atlassian.Templates.Automation.JIRA.jqlFilter", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public String getViewTemplate(final TriggerConfiguration triggerConfiguration, final String actor)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, triggerConfiguration);
            return soyTemplateRenderer.render(CONFIG_COMPLETE_KEY, "Atlassian.Templates.Automation.JIRA.jqlFilterView", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public ErrorCollection validateAddConfiguration(final I18nResolver i18n, final Map<String, List<String>> params, final String actor)
    {
        final ErrorCollection errorCollection = super.validateAddConfiguration(i18n, params, actor);
        final String jqlString = singleValue(params, JQL_KEY);
        if (StringUtils.isBlank(jqlString))
        {
            // No parsing is done here, as the JQL needs to be verified before each execution and at this point we don't have actor here
            errorCollection.addError(JQL_KEY, i18n.getText("automation.jira.jql.invalid"));
        }
        else
        {
            final User user = userManager.getUser(actor);
            final SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlString);

            if (!parseResult.isValid())
            {
                errorCollection.addError(JQL_KEY, i18n.getText("automation.jira.jql.invalid"));
            }
            else
            {
                // Validation is now here, because here we still have HTTP session to validate the JQL (see JRA-33499)
                final MessageSet messageSet = searchService.validateQuery(user, parseResult.getQuery());
                if (messageSet.hasAnyErrors())
                {
                    errorCollection.addError(JQL_KEY, messageSet.getErrorMessages().toString());
                }
                else if (messageSet.hasAnyWarnings())
                {
                    errorCollection.addError(JQL_KEY, messageSet.getWarningMessages().toString());
                }
            }
        }

        final String maxResults = singleValue(params, MAX_RESULTS_KEY);
        if (!StringUtils.isBlank(maxResults) && (!StringUtils.isNumeric(maxResults) || Integer.parseInt(maxResults) < 0))
        {
            errorCollection.addError(MAX_RESULTS_KEY, i18n.getText("automation.jira.jql.maxResults.invalid"));
        }
        return errorCollection;
    }

    private Query tryParseQuery(User user, String jqlString, ErrorCollection errorCollection)
    {
        final SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlString);
        if (parseResult.isValid())
        {
            // Disabled validation of the query until JRA-33499 is solved
            /*
            final MessageSet messageSet = searchService.validateQuery(user, parseResult.getQuery());
            if (messageSet.hasAnyErrors())
            {
                errorCollection.addErrorMessages(messageSet.getErrorMessages());
                return null;
            }*/
            return parseResult.getQuery();
        }

        errorCollection.addErrorMessage("Invalid JQL: " + jqlString);
        return null;
    }
}

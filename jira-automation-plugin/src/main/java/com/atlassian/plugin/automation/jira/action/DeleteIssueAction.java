package com.atlassian.plugin.automation.jira.action;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.plugin.automation.core.Action;
import com.atlassian.plugin.automation.core.action.ActionConfiguration;
import com.atlassian.plugin.automation.core.auditlog.AuditString;
import com.atlassian.plugin.automation.core.auditlog.DefaultAuditString;
import com.atlassian.plugin.automation.jira.util.ErrorCollectionUtil;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.plugin.automation.util.ParameterUtil;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

import static com.atlassian.plugin.automation.jira.util.Constants.CONFIG_COMPLETE_KEY;
import static com.atlassian.plugin.automation.util.ParameterUtil.singleValue;

/*
    Implements action which deletes an issue
 */
public class DeleteIssueAction implements Action<Issue>
{
    public static final String DELETE_NOTIFICATION_KEY = "jiraDeleteIssueNotification";

    private static final Logger log = Logger.getLogger(DeleteIssueAction.class);

    private SoyTemplateRenderer soyTemplateRenderer;
    private UserManager userManager;
    private IssueService issueService;
    private int deletedIssuesCount;
    private boolean sendNotification;

    @Inject
    public DeleteIssueAction(@ComponentImport final SoyTemplateRenderer soyTemplateRenderer,
                             @ComponentImport final UserManager userManager,
                             @ComponentImport final IssueService issueService)
    {
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.userManager = userManager;
        this.issueService = issueService;
    }

    @Override
    public void init(ActionConfiguration config)
    {
        sendNotification = Boolean.parseBoolean(singleValue(config, DELETE_NOTIFICATION_KEY));
    }

    @Override
    public void execute(String actor, Iterable<Issue> items, ErrorCollection errorCollection)
    {
        log.debug("Deleting Issues: " + Iterables.size(items));

        deletedIssuesCount = 0;
        final User directoryUser = userManager.getUserByName(actor).getDirectoryUser();
        for (Issue issue : items)
        {
            final com.atlassian.jira.util.ErrorCollection jiraErrorCollection = new SimpleErrorCollection();
            try
            {
                IssueService.DeleteValidationResult deleteValidationResult = issueService.validateDelete(directoryUser, issue.getId());
                if (deleteValidationResult.isValid())
                {
                    log.debug("Deleting issue: " + issue.getKey());
                    final com.atlassian.jira.util.ErrorCollection deleteResult = issueService.delete(directoryUser,
                            deleteValidationResult, EventDispatchOption.ISSUE_DELETED, sendNotification);
                    if (deleteResult.hasAnyErrors())
                    {
                        log.debug("Unable to delete issue: " + issue.getKey());
                        jiraErrorCollection.addErrorCollection(deleteResult);
                    }
                    else
                    {
                        deletedIssuesCount++;
                        log.debug("Successfully deleted issue: " + issue.getKey());
                    }
                }
                else
                {
                    jiraErrorCollection.addErrorCollection(deleteValidationResult.getErrorCollection());
                }
            }
            catch (Exception e)
            {
                log.error("Unable to delete issue: " + issue.getKey(), e);
                jiraErrorCollection.addErrorMessage("Exception occurred when deleting issue: " + issue.getKey());
            }

            if (jiraErrorCollection.hasAnyErrors())
            {
                log.error(String.format("Unable to delete issue '%s' using actor '%s': %s", issue.getKey(), actor, jiraErrorCollection));
                errorCollection.addErrorCollection(ErrorCollectionUtil.transform(jiraErrorCollection));
            }
        }
        log.debug("Deleting Issues finished");
    }

    @Override
    public AuditString getAuditLog()
    {
        return new DefaultAuditString(String.format("Deleted %d Issues", deletedIssuesCount));
    }

    @Override
    public String getConfigurationTemplate(ActionConfiguration actionConfiguration, String actor)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, actionConfiguration);

            return soyTemplateRenderer.render(CONFIG_COMPLETE_KEY, "Atlassian.Templates.Automation.JIRA.deleteIssueAction", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public String getViewTemplate(ActionConfiguration actionConfiguration, String s)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, actionConfiguration);

            return soyTemplateRenderer.render(CONFIG_COMPLETE_KEY, "Atlassian.Templates.Automation.JIRA.deleteIssueActionView", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public ErrorCollection validateAddConfiguration(I18nResolver i18n, Map<String, List<String>> params, String actor)
    {
        return new ErrorCollection();
    }
}

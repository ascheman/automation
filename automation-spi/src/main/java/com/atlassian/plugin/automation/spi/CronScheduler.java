package com.atlassian.plugin.automation.spi;

import com.atlassian.sal.api.scheduling.PluginJob;

import java.util.Map;

/**
 * Extension to SAL's {@link com.atlassian.sal.api.scheduling.PluginScheduler}
 */
public interface CronScheduler
{
    /**
     * Schedules a job based on a cron expression.  This will eventually be implemented in SAL for all products.
     */
    void scheduleJob(String jobKey, Class<? extends PluginJob> jobClass, Map<String, Object> jobDataMap, String cronExpression);


    /**
     * Unschedule the given job. This is mainly here for convenience and will simply call through to SAL's {@link
     * com.atlassian.sal.api.scheduling.PluginScheduler#unscheduleJob(String)}.
     */
    void unscheduleJob(String jobKey);
}

package com.atlassian.plugin.automation.spi;

/**
 * SPI for setup/teardown of running a job on different thread in products
 */
public interface ThreadLocalContextProvider
{
    /**
     * Provides setup of local context for running jobs in separate thread
     *
     * @param actor
     */
    void preCall(String actor);

    /**
     * Provides teardown of local context after running jobs in separate thread
     *
     * @param actor
     */
    void postCall(String actor);
}
